package usac.ipc1.start;

import usac.ipc1.Principal;
import usac.ipc1.beans.*;
import usac.ipc1.beans.Transaccion;
import usac.ipc1.utils.Utility;
import usac.ipc1.views.LoginView;

import javax.swing.*;

public class Init {

    public Init(){

        Principal.vectorCliente = new Cliente[100];
        Principal.vectorAgencia = new Agencia[100];
        Principal.vectorAutobanco = new Autobanco[100];
        Principal.vectorCajero = new Cajero[100];
        Principal.vectorEmpleado = new Empleado[100];
        Principal.vectorCuenta = new Cuenta[100];
        Principal.callCenter = new CallCenter();
        Principal.vectorTransaccion = new Transaccion[100];
        Principal.vectorTarjeta = new TarjetaCredito[100];
        Principal.vectorPrestamo = new Prestamo[100];
        Utility utility = new Utility();
        utility.startApp();

        SwingUtilities.invokeLater(()-> new LoginView());
    }

}
