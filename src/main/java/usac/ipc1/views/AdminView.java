package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.Cliente;
import usac.ipc1.dialogs.*;
import usac.ipc1.pdf.PdfGeneration;
import usac.ipc1.utils.Utility;

import javax.rmi.CORBA.Util;
import javax.swing.*;
import java.awt.*;

public class AdminView extends JFrame {

    private JPanel lateralMenu, principalContainer;
    private JButton btnClientes, btnAgencia, btnAutobancos, btnCajeros, btnSession, btnEmpleado, btnTarjetas, btnPrestamos, btnReportes;
    private JButton btnLAgencias, btnLCajeros, btnLClientes, btnTopClientesC, btnTopClientesD, btnTopClientesDeuda, btnTopAgencia;
    private JButton btnTopOperacionesCC, btnSumaEfectivo, btnListaCliente, btnEfectivoPerA, btnLEmpleados, btnLEmpleadosC, btnAgenciaMoreEmpl, btnComprasTarjeta;

    public AdminView() {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(800, 500));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Administracion BDE");

        createLateralMenu();
        panelPrincipal(Principal.vectorCliente, Table.TipoTabla.CLIENTE);

        add(lateralMenu, BorderLayout.WEST);

        setVisible(true);
    }

    private void createLateralMenu() {

        btnClientes = new JButton();
        btnClientes = Utility.lateralButtonProperties(btnClientes, "Clientes", this);
        btnClientes.addActionListener(e -> panelPrincipal(Principal.vectorCliente, Table.TipoTabla.CLIENTE));

        btnAgencia = new JButton();
        btnAgencia = Utility.lateralButtonProperties(btnAgencia, "Agencias", this);
        btnAgencia.addActionListener(e -> panelPrincipal(Principal.vectorAgencia, Table.TipoTabla.AGENCIA));

        btnAutobancos = new JButton();
        btnAutobancos = Utility.lateralButtonProperties(btnAutobancos, "Autobancos", this);
        btnAutobancos.addActionListener(e -> panelPrincipal(Principal.vectorAutobanco, Table.TipoTabla.AUTOBANCO));

        btnCajeros = new JButton();
        btnCajeros = Utility.lateralButtonProperties(btnCajeros, "Cajeros", this);
        btnCajeros.addActionListener(e -> panelPrincipal(Principal.vectorCajero, Table.TipoTabla.CAJERO));

        btnEmpleado = new JButton();
        btnEmpleado = Utility.lateralButtonProperties(btnEmpleado, "Empleados", this);
        btnEmpleado.addActionListener(e -> panelPrincipal(Principal.vectorEmpleado, Table.TipoTabla.EMPLEADO));

        btnTarjetas = new JButton();
        btnTarjetas = Utility.lateralButtonProperties(btnTarjetas, "Tarjetas", this);
        btnTarjetas.addActionListener(e -> panelPrincipal(Principal.vectorTarjeta, Table.TipoTabla.TARJETA));

        btnPrestamos = new JButton();
        btnPrestamos = Utility.lateralButtonProperties(btnPrestamos, "Prestamos", this);
        btnPrestamos.addActionListener(e -> panelPrincipal(Principal.vectorPrestamo, Table.TipoTabla.PRESTAMO));

        btnReportes = new JButton();
        btnReportes = Utility.lateralButtonProperties(btnReportes, "Reportes", this);
        btnReportes.addActionListener(e -> panelReportes());

        btnSession = new JButton();
        btnSession = Utility.lateralButtonProperties(btnSession, "Cerrar Sesión", this);
        btnSession.addActionListener(e -> {
            dispose();
            new LoginView();
        });

        lateralMenu = new JPanel();
        lateralMenu.setLayout(new BoxLayout(lateralMenu, BoxLayout.Y_AXIS));

        lateralMenu.add(Box.createVerticalGlue());
        lateralMenu.add(btnClientes);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnAgencia);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnAutobancos);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnCajeros);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnEmpleado);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnTarjetas);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnPrestamos);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnReportes);
        lateralMenu.add(Box.createRigidArea(new Dimension(0, 1)));
        lateralMenu.add(btnSession);
        lateralMenu.add(Box.createVerticalGlue());

        lateralMenu.setBackground(Color.decode("#407F7F"));
        lateralMenu.setPreferredSize(new Dimension(150, 0));

    }

    private void panelPrincipal(Object vector[], Table.TipoTabla tipoTabla) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        Table tabla = new Table(vector, tipoTabla);
        JScrollPane scrollPane = new JScrollPane(tabla);
        scrollPane.setMaximumSize(new Dimension(1500, 184));

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JLabel lblSearch = new JLabel("Buscar");
        JTextField search = new JTextField();
        search.setMaximumSize(new Dimension(getWidth(), 30));
        search.addActionListener(e -> Utility.search(search.getText(), tipoTabla, this));

        JButton addNew = new JButton();
        addNew = Utility.lateralButtonProperties(addNew, "Nuevo", this);
        addNew.setBackground(Color.decode("#99cc33"));
        addNew.addActionListener(e -> {
            switch (tipoTabla) {
                case CLIENTE:
                    new ClienteDialog(AdminView.this, false, Principal.vectorCliente, tabla).setVisible(true);
                    break;

                case AGENCIA:
                    new AgenciaDialog(AdminView.this, false, Principal.vectorAgencia, tabla).setVisible(true);
                    break;

                case AUTOBANCO:
                    new AutobancoDialog(AdminView.this, false, Principal.vectorAutobanco, tabla).setVisible(true);
                    break;

                case CAJERO:
                    new CajeroDialog(AdminView.this, false, Principal.vectorCajero, tabla).setVisible(true);
                    break;

                case EMPLEADO:
                    new EmpleadoDialog(AdminView.this, false, Principal.vectorEmpleado, tabla).setVisible(true);
                    break;
            }
        });

        JButton edit = new JButton();
        edit = Utility.lateralButtonProperties(edit, "Editar", this);
        edit.setBackground(Color.decode("#ff9966"));
        edit.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                switch (tipoTabla) {
                    case CLIENTE:
                        new ClienteDialog(AdminView.this, false, Principal.vectorCliente, tabla, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case AGENCIA:
                        new AgenciaDialog(AdminView.this, false, Principal.vectorAgencia, tabla, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case AUTOBANCO:
                        new AutobancoDialog(AdminView.this, false, Principal.vectorAutobanco, tabla, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case CAJERO:
                        new CajeroDialog(AdminView.this, false, Principal.vectorCajero, tabla, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case EMPLEADO:
                        new EmpleadoDialog(AdminView.this, false, Principal.vectorEmpleado, tabla, tabla.getSelectedRow()).setVisible(true);
                        break;
                }
            } else {
                JOptionPane.showMessageDialog(AdminView.this,
                        "Debe seleccionar la fila a editar",
                        "Editar fila",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        JButton deleteSelected = new JButton();
        deleteSelected = Utility.lateralButtonProperties(deleteSelected, "Eliminar", this);
        deleteSelected.setBackground(Color.decode("#cc3300"));
        deleteSelected.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                if (JOptionPane.YES_OPTION ==
                        JOptionPane.showConfirmDialog(AdminView.this,
                                "Desea eliminar la fila seleccionada?",
                                "Eliminar fila",
                                JOptionPane.YES_NO_OPTION)) {

                    tabla.eliminar(tabla.getSelectedRow(), tipoTabla);
                }
            } else {
                JOptionPane.showMessageDialog(AdminView.this,
                        "Debe seleccionar la fila a eliminar",
                        "Eliminar fila",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        JButton asignSelected = new JButton();
        asignSelected = Utility.lateralButtonProperties(asignSelected, "Asignar", this);
        asignSelected.setBackground(Color.decode("#3366cc"));
        asignSelected.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                Utility.asignar(tabla.getSelectedRow(), this);
            } else {
                JOptionPane.showMessageDialog(AdminView.this,
                        "Debe seleccionar el empleado a asignar",
                        "Asignar empleado",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        JButton generatePdf = new JButton();
        generatePdf = Utility.lateralButtonProperties(generatePdf, "Generar PDF", this);
        generatePdf.setBackground(Color.decode("#3366cc"));
        generatePdf.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                new PdfGeneration(Principal.vectorCliente[tabla.getSelectedRow()]);
            } else {
                JOptionPane.showMessageDialog(AdminView.this,
                        "Debe seleccionar un cliente",
                        "PDF Cliente",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        JPanel buttonsPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(buttonsPanel, BoxLayout.X_AXIS);
        buttonsPanel.setLayout(boxLayout);
        buttonsPanel.add(addNew);
        buttonsPanel.add(edit);
        buttonsPanel.add(deleteSelected);

        switch (tipoTabla) {
            case EMPLEADO:
                buttonsPanel.add(asignSelected);
                break;

            case CLIENTE:
                buttonsPanel.add(generatePdf);
                break;

            case TARJETA:
            case PRESTAMO:
                lblSearch.setVisible(false);
                search.setVisible(false);
                addNew.setVisible(false);
                edit.setVisible(false);
                deleteSelected.setVisible(false);

                JButton btnAprobar = new JButton();
                btnAprobar = Utility.lateralButtonProperties(btnAprobar, "Aprobar", this);
                btnAprobar.setBackground(Color.decode("#99cc33"));
                btnAprobar.addActionListener(e -> {
                    if (tabla.getSelectedRowCount() > 0) {
                        tabla.aprobar(tabla.getSelectedRow(), tipoTabla);
                    } else {
                        JOptionPane.showMessageDialog(AdminView.this,
                                "Debe seleccionar una fila",
                                "Aprobar",
                                JOptionPane.WARNING_MESSAGE);
                    }
                });

                JButton btnDenegar = new JButton();
                btnDenegar = Utility.lateralButtonProperties(btnDenegar, "Denegar", this);
                btnDenegar.setBackground(Color.decode("#cc3300"));
                btnDenegar.addActionListener(e -> {
                    if (tabla.getSelectedRowCount() > 0) {
                        tabla.denegar(tabla.getSelectedRow(), tipoTabla);
                    } else {
                        JOptionPane.showMessageDialog(AdminView.this,
                                "Debe seleccionar una fila",
                                "Denegar",
                                JOptionPane.WARNING_MESSAGE);
                    }
                });

                buttonsPanel.add(btnAprobar);
                buttonsPanel.add(btnDenegar);

                break;
        }


        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(lblSearch)
                        .addComponent(search)
                        .addComponent(scrollPane)
                        .addComponent(buttonsPanel)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(lblSearch)
                        .addComponent(search)
                        .addComponent(scrollPane)
                        .addComponent(buttonsPanel)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    private void panelReportes() {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        btnLAgencias = new JButton();
        btnLAgencias = Utility.lateralButtonProperties(btnLAgencias, "Listado de agencias", this);
        btnLAgencias.setBackground(Color.decode("#2ea2de"));
        btnLAgencias.addActionListener(e -> new PdfGeneration(1));

        btnLCajeros = new JButton();
        btnLCajeros = Utility.lateralButtonProperties(btnLCajeros, "Listado de cajeros", this);
        btnLCajeros.setBackground(Color.decode("#2ea2de"));
        btnLCajeros.addActionListener(e -> new PdfGeneration(2));

        btnLClientes = new JButton();
        btnLClientes = Utility.lateralButtonProperties(btnLClientes, "Listado de clientes", this);
        btnLClientes.setBackground(Color.decode("#2ea2de"));
        btnLClientes.addActionListener(e -> new PdfGeneration(3));

        btnTopClientesC = new JButton();
        btnTopClientesC = Utility.lateralButtonProperties(btnTopClientesC, "Top 3 clientes con mas cuentas", this);
        btnTopClientesC.setBackground(Color.decode("#2ea2de"));
        btnTopClientesC.addActionListener(e -> new PdfGeneration(4));

        btnTopClientesD = new JButton();
        btnTopClientesD = Utility.lateralButtonProperties(btnTopClientesD, "Top 3 clientes con mas dinero", this);
        btnTopClientesD.setBackground(Color.decode("#2ea2de"));
        btnTopClientesD.addActionListener(e -> new PdfGeneration(5));

        btnTopClientesDeuda = new JButton();
        btnTopClientesDeuda = Utility.lateralButtonProperties(btnTopClientesDeuda, "Top 3 clientes con mas deudas", this);
        btnTopClientesDeuda.setBackground(Color.decode("#2ea2de"));
        btnTopClientesDeuda.addActionListener(e -> new PdfGeneration(6));

        btnTopAgencia = new JButton();
        btnTopAgencia = Utility.lateralButtonProperties(btnTopAgencia, "Top 3 agencias mas utilizadas", this);
        btnTopAgencia.setBackground(Color.decode("#2ea2de"));
        btnTopAgencia.addActionListener(e -> new PdfGeneration(7));

        btnTopOperacionesCC = new JButton();
        btnTopOperacionesCC = Utility.lateralButtonProperties(btnTopOperacionesCC, "Top 2 operaciones Call-Center", this);
        btnTopOperacionesCC.setBackground(Color.decode("#2ea2de"));
        btnTopOperacionesCC.addActionListener(e -> new PdfGeneration(8));

        btnSumaEfectivo = new JButton();
        btnSumaEfectivo = Utility.lateralButtonProperties(btnSumaEfectivo, "Suma de efectivo de agencias", this);
        btnSumaEfectivo.setBackground(Color.decode("#2ea2de"));
        btnSumaEfectivo.addActionListener(e -> new PdfGeneration(9));

        btnEfectivoPerA = new JButton();
        btnEfectivoPerA = Utility.lateralButtonProperties(btnEfectivoPerA, "Efectivo de agencias", this);
        btnEfectivoPerA.setBackground(Color.decode("#2ea2de"));
        btnEfectivoPerA.addActionListener(e -> new PdfGeneration(10));

        btnLEmpleados = new JButton();
        btnLEmpleados = Utility.lateralButtonProperties(btnLEmpleados, "Listado de empleados de agencias", this);
        btnLEmpleados.setBackground(Color.decode("#2ea2de"));
        btnLEmpleados.addActionListener(e -> new PdfGeneration(11));

        btnLEmpleadosC = new JButton();
        btnLEmpleadosC = Utility.lateralButtonProperties(btnLEmpleadosC, "Listado de empleados de central", this);
        btnLEmpleadosC.setBackground(Color.decode("#2ea2de"));
        btnLEmpleadosC.addActionListener(e -> new PdfGeneration(12));

        btnAgenciaMoreEmpl = new JButton();
        btnAgenciaMoreEmpl = Utility.lateralButtonProperties(btnAgenciaMoreEmpl, "Agencia con mas empleados", this);
        btnAgenciaMoreEmpl.setBackground(Color.decode("#2ea2de"));
        btnAgenciaMoreEmpl.addActionListener(e -> new PdfGeneration(13));

        btnComprasTarjeta = new JButton();
        btnComprasTarjeta = Utility.lateralButtonProperties(btnComprasTarjeta, "Top 3 clientes con mas compras", this);
        btnComprasTarjeta.setBackground(Color.decode("#2ea2de"));
        btnComprasTarjeta.addActionListener(e -> new PdfGeneration(14));

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnLAgencias)
                        .addComponent(btnLCajeros)
                        .addComponent(btnLClientes)
                        .addComponent(btnTopClientesC)
                        .addComponent(btnTopClientesD)
                        .addComponent(btnTopClientesDeuda)
                        .addComponent(btnTopAgencia)
                        .addComponent(btnTopOperacionesCC)
                        .addComponent(btnSumaEfectivo)
                        .addComponent(btnEfectivoPerA)
                        .addComponent(btnLEmpleados)
                        .addComponent(btnLEmpleadosC)
                        .addComponent(btnAgenciaMoreEmpl)
                        .addComponent(btnComprasTarjeta)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnLAgencias)
                        .addComponent(btnLCajeros)
                        .addComponent(btnLClientes)
                        .addComponent(btnTopClientesC)
                        .addComponent(btnTopClientesD)
                        .addComponent(btnTopClientesDeuda)
                        .addComponent(btnTopAgencia)
                        .addComponent(btnTopOperacionesCC)
                        .addComponent(btnSumaEfectivo)
                        .addComponent(btnEfectivoPerA)
                        .addComponent(btnLEmpleados)
                        .addComponent(btnLEmpleadosC)
                        .addComponent(btnAgenciaMoreEmpl)
                        .addComponent(btnComprasTarjeta)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

}
