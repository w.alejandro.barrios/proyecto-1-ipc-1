package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.*;
import usac.ipc1.utils.Utility;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

public class OperacionesView extends JFrame {

    private JPanel principalContainer;
    private JButton btnDeposito, btnRetiro, btnCambioCheques, btnPagoServicios;
    private JButton btnPagoTarjeta, btnPrestamo, btnRetiroEfectivo, btnConfirmar, btnConsultaSaldo, btnSolicitarTarjeta, btnSolicitarPrestamo;
    private JComboBox cbUbicacion;

    public OperacionesView(Cliente cliente, JFrame frame) {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(800, 500));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Operaciones BDE");

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            dispose();
            new ClienteChooseView();
        });

        JLabel lblMonto = new JLabel("Monto Inicial");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));

        JLabel lblTipo = new JLabel("Tipo de cuenta");
        JComboBox cbTipo = new JComboBox();
        cbTipo.setMaximumSize(new Dimension(getWidth(), 40));
        cbTipo.addItem("Monetaria");
        cbTipo.addItem("Ahorro");

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Procesar", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            Principal.vectorCuenta[Principal.contadorCuenta] = new Cuenta();
            Principal.vectorCuenta[Principal.contadorCuenta] = new Cuenta();
            Principal.vectorCuenta[Principal.contadorCuenta].setCliente(cliente);
            Principal.vectorCuenta[Principal.contadorCuenta].setMontoInicial(Double.parseDouble(txtMonto.getText()));
            Principal.vectorCuenta[Principal.contadorCuenta].setMontoFinal(Double.parseDouble(txtMonto.getText()));
            Principal.vectorCuenta[Principal.contadorCuenta].setNoCuenta(Principal.contadorCuenta);
            if (cbTipo.getSelectedItem().equals("Monetaria"))
                Principal.vectorCuenta[Principal.contadorCuenta].setTipoCuenta(true);
            else
                Principal.vectorCuenta[Principal.contadorCuenta].setTipoCuenta(false);
            Principal.vectorCuenta[Principal.contadorCuenta].setFechaApertura(new Date());
            Principal.contadorCuenta++;

            JOptionPane.showMessageDialog(frame,
                    "Cuenta creada exitosamente",
                    "Crear Cuenta",
                    JOptionPane.WARNING_MESSAGE);
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();

        setVisible(true);
    }

    public OperacionesView(int selection) {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(800, 500));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Operaciones BDE");

        if (selection == 100)
            simularCompra(selection);
        else
            selectionPanel(selection);

        setVisible(true);
    }

    //region SelectionOperation
    private void selectionPanel(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            dispose();
            new ClienteChooseView();
        });

        btnDeposito = new JButton();
        btnDeposito = Utility.lateralButtonProperties(btnDeposito, "Deposito", this);
        btnDeposito.setBackground(Color.decode("#2ea2de"));
        btnDeposito.addActionListener(e -> depositoPanel(selection));

        btnRetiro = new JButton();
        btnRetiro = Utility.lateralButtonProperties(btnRetiro, "Retiro", this);
        btnRetiro.setBackground(Color.decode("#ed7f01"));
        btnRetiro.addActionListener(e -> retiroPanel(selection));

        btnCambioCheques = new JButton();
        btnCambioCheques = Utility.lateralButtonProperties(btnCambioCheques, "Cambiar cheque", this);
        btnCambioCheques.setBackground(Color.decode("#bb12ec"));
        btnCambioCheques.addActionListener(e -> cambioCheques(selection));

        btnPagoServicios = new JButton();
        btnPagoServicios = Utility.lateralButtonProperties(btnPagoServicios, "Pago servicio", this);
        btnPagoServicios.setBackground(Color.decode("#84de00"));
        btnPagoServicios.addActionListener(e -> pagoServiciosPanel(selection));

        btnPagoTarjeta = new JButton();
        btnPagoTarjeta = Utility.lateralButtonProperties(btnPagoTarjeta, "Pago tarjeta", this);
        btnPagoTarjeta.setBackground(Color.decode("#2ea2de"));
        btnPagoTarjeta.addActionListener(e -> pagoTarjetaPanel(selection));

        btnPrestamo = new JButton();
        btnPrestamo = Utility.lateralButtonProperties(btnPrestamo, "Pago prestamo", this);
        btnPrestamo.setBackground(Color.decode("#ed7f01"));
        btnPrestamo.addActionListener(e -> pagoPrestamoPanel(selection));

        btnRetiroEfectivo = new JButton();
        btnRetiroEfectivo = Utility.lateralButtonProperties(btnRetiroEfectivo, "Retiro Efectivo", this);
        btnRetiroEfectivo.setBackground(Color.decode("#bb12ec"));
        btnRetiroEfectivo.addActionListener(e -> retiroPanel(selection));

        btnConsultaSaldo = new JButton();
        btnConsultaSaldo = Utility.lateralButtonProperties(btnConsultaSaldo, "Consulta de saldo", this);
        btnConsultaSaldo.setBackground(Color.decode("#84de00"));
        btnConsultaSaldo.addActionListener(e -> consultaPanel(selection));

        btnSolicitarTarjeta = new JButton();
        btnSolicitarTarjeta = Utility.lateralButtonProperties(btnSolicitarTarjeta, "Solicitar tarjeta", this);
        btnSolicitarTarjeta.setBackground(Color.decode("#2ea2de"));
        btnSolicitarTarjeta.addActionListener(e -> solicitarTarjeta(selection));

        btnSolicitarPrestamo = new JButton();
        btnSolicitarPrestamo = Utility.lateralButtonProperties(btnSolicitarPrestamo, "Solicitar prestamo", this);
        btnSolicitarPrestamo.setBackground(Color.decode("#ed7f01"));
        btnSolicitarPrestamo.addActionListener(e -> solicitarPrestamo(selection));

        if (selection == 1 || selection == 2) {
            btnRetiroEfectivo.setVisible(false);
            btnConsultaSaldo.setVisible(false);
        }

        if (selection == 3) {
            btnDeposito.setVisible(false);
            btnRetiro.setVisible(false);
            btnCambioCheques.setVisible(false);
            btnPagoServicios.setVisible(false);
            btnPagoTarjeta.setVisible(false);
            btnPrestamo.setVisible(false);
            btnSolicitarTarjeta.setVisible(false);
            btnSolicitarPrestamo.setVisible(false);
        }

        if (selection == 4) {
            btnRetiroEfectivo.setVisible(false);
            btnConsultaSaldo.setVisible(false);
            btnDeposito.setVisible(false);
            btnCambioCheques.setVisible(false);
            btnRetiro.setVisible(false);
            btnSolicitarTarjeta.setVisible(false);
            btnSolicitarPrestamo.setVisible(false);
        }

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(btnDeposito)
                        .addComponent(btnRetiro)
                        .addComponent(btnCambioCheques)
                        .addComponent(btnPagoServicios)
                        .addComponent(btnPagoTarjeta)
                        .addComponent(btnPrestamo)
                        .addComponent(btnRetiroEfectivo)
                        .addComponent(btnConsultaSaldo)
                        .addComponent(btnSolicitarTarjeta)
                        .addComponent(btnSolicitarPrestamo)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(btnDeposito)
                        .addComponent(btnRetiro)
                        .addComponent(btnCambioCheques)
                        .addComponent(btnPagoServicios)
                        .addComponent(btnPagoTarjeta)
                        .addComponent(btnPrestamo)
                        .addComponent(btnRetiroEfectivo)
                        .addComponent(btnConsultaSaldo)
                        .addComponent(btnSolicitarTarjeta)
                        .addComponent(btnSolicitarPrestamo)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();

    }
    //endregion

    //region deposito
    private void depositoPanel(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> selectionPanel(selection));

        JLabel lblCuenta = new JLabel("Cta. Deposito");
        JTextField txtCuenta = new JTextField();
        txtCuenta.setMaximumSize(new Dimension(getWidth(), 40));

        JLabel lblMonto = new JLabel("Monto a depositar");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));

        JLabel lblTipo = new JLabel("Tipo Deposito");
        JComboBox cbTipo = new JComboBox();
        cbTipo.setMaximumSize(new Dimension(getWidth(), 40));
        cbTipo.addItem("Efectivo");
        cbTipo.addItem("Cheque");

        JLabel lblUbicacion = new JLabel("Ubicacion");
        cbUbicacion = new JComboBox();
        cbUbicacion.setMaximumSize(new Dimension(getWidth(), 40));
        cbUbicacion = Utility.buildCombo(cbUbicacion, selection);

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Procesar", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            boolean successful = false;
            for (int i = 0; i < Principal.vectorCuenta.length; i++) {
                if (Principal.vectorCuenta[i] != null) {
                    if (Principal.vectorCuenta[i].getNoCuenta() == Integer.parseInt(txtCuenta.getText())) {
                        Principal.vectorCuenta[i].setMontoFinal(Principal.vectorCuenta[i].getMontoFinal() + Double.parseDouble(txtMonto.getText()));
                        Transaccion transaction = new Transaccion();
                        transaction.setId(Principal.contadorTransaccion);
                        transaction.setCliente(Principal.clienteChoose);
                        transaction.setFechaTransaccion(new Date());
                        transaction.setMonto(Double.parseDouble(txtMonto.getText()));
                        if (cbTipo.getSelectedItem().equals("Efectivo"))
                            transaction.setTipo(true);
                        else
                            transaction.setTipo(false);
                        transaction.setTipoOperacion("Deposito");
                        switch (selection) {
                            case 1:
                                for (int o = 0; o < Principal.vectorAgencia.length; o++) {
                                    if (Principal.vectorAgencia[o] != null) {
                                        if (Principal.vectorAgencia[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                            Principal.vectorAgencia[o].setEfectivo(Principal.vectorAgencia[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                                        }
                                    }
                                }
                                break;

                            case 2:
                                for (int o = 0; o < Principal.vectorAutobanco.length; o++) {
                                    if (Principal.vectorAutobanco[o] != null) {
                                        if (Principal.vectorAutobanco[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                            Principal.vectorAutobanco[o].setEfectivo(Principal.vectorAutobanco[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                                        }
                                    }
                                }
                                break;

                        }
                        Utility.searchAndSetLocation(cbUbicacion.getSelectedItem().toString(), selection, transaction);
                        transaction.setCuentaDestino(Principal.vectorCuenta[i].getNoCuenta());
                        Principal.vectorTransaccion[Principal.contadorTransaccion] = transaction;
                        successful = true;
                        Principal.contadorTransaccion++;
                    }
                }

            }
            messageConfirmation(successful);
            txtCuenta.setText("");
            txtMonto.setText("");
            cbTipo.setSelectedItem("Efectivo");
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblCuenta)
                        .addComponent(txtCuenta)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblCuenta)
                        .addComponent(txtCuenta)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    //region retiro
    private void retiroPanel(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> selectionPanel(selection));

        JLabel lblCuenta = new JLabel("Cta. Retiro");
        JComboBox cbCuentas = new JComboBox();
        cbCuentas.setMaximumSize(new Dimension(getWidth(), 40));
        for (int i = 0; i < Principal.vectorCuenta.length; i++) {
            if (Principal.vectorCuenta[i] != null) {
                if (Principal.vectorCuenta[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                    cbCuentas.addItem(Principal.vectorCuenta[i].getNoCuenta());
                }
            }
        }

        JLabel lblMonto = new JLabel("Monto a retirar");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));

        JLabel lblUbicacion = new JLabel("Ubicacion");
        cbUbicacion = new JComboBox();
        cbUbicacion.setMaximumSize(new Dimension(getWidth(), 40));
        cbUbicacion = Utility.buildCombo(cbUbicacion, selection);

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Procesar", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            boolean successful = false;
            for (int i = 0; i < Principal.vectorCuenta.length; i++) {
                if (Principal.vectorCuenta[i] != null) {
                    if (Principal.vectorCuenta[i].getNoCuenta() == Integer.parseInt(cbCuentas.getSelectedItem().toString())) {
                        Principal.vectorCuenta[i].setMontoFinal(Principal.vectorCuenta[i].getMontoFinal() - Double.parseDouble(txtMonto.getText()));
                        Transaccion transaction = new Transaccion();
                        transaction.setId(Principal.contadorTransaccion);
                        transaction.setCliente(Principal.clienteChoose);
                        transaction.setFechaTransaccion(new Date());
                        transaction.setMonto(Double.parseDouble(txtMonto.getText()));
                        transaction.setTipoOperacion("Retiro");
                        switch (selection) {
                            case 1:
                                for (int o = 0; o < Principal.vectorAgencia.length; o++) {
                                    if (Principal.vectorAgencia[o] != null) {
                                        if (Principal.vectorAgencia[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                            Principal.vectorAgencia[o].setEfectivo(Principal.vectorAgencia[o].getEfectivo() - Double.parseDouble(txtMonto.getText()));
                                        }
                                    }
                                }
                                break;

                            case 2:
                                for (int o = 0; o < Principal.vectorAutobanco.length; o++) {
                                    if (Principal.vectorAutobanco[o] != null) {
                                        if (Principal.vectorAutobanco[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                            Principal.vectorAutobanco[o].setEfectivo(Principal.vectorAutobanco[o].getEfectivo() - Double.parseDouble(txtMonto.getText()));
                                        }
                                    }
                                }
                                break;

                            case 3:
                                for (int o = 0; o < Principal.vectorCajero.length; o++) {
                                    if (Principal.vectorCajero[o] != null) {
                                        if (Principal.vectorCajero[o].getId() == Integer.parseInt(cbUbicacion.getSelectedItem().toString())) {
                                            Principal.vectorCajero[o].setEfectivo(Principal.vectorCajero[o].getEfectivo() - Double.parseDouble(txtMonto.getText()));
                                            Principal.vectorCajero[o].setNoTransaccionesProcesadas(Principal.vectorCajero[0].getNoTransaccionesProcesadas() + 1);
                                        }
                                    }
                                }
                                break;

                        }
                        Utility.searchAndSetLocation(cbUbicacion.getSelectedItem().toString(), selection, transaction);
                        transaction.setCuentaRetiro(Principal.vectorCuenta[i].getNoCuenta());
                        Principal.vectorTransaccion[Principal.contadorTransaccion] = transaction;
                        successful = true;
                        Principal.contadorTransaccion++;
                    }
                }

            }
            messageConfirmation(successful);
            txtMonto.setText("");
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblCuenta)
                        .addComponent(cbCuentas)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblCuenta)
                        .addComponent(cbCuentas)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    //region consultaSaldo
    private void consultaPanel(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> selectionPanel(selection));

        JLabel lblCuentas = new JLabel("Cuenta No.");
        JComboBox cbCuentas = new JComboBox();
        cbCuentas.setMaximumSize(new Dimension(getWidth(), 40));
        for (int i = 0; i < Principal.vectorCuenta.length; i++) {
            if (Principal.vectorCuenta[i] != null) {
                if (Principal.vectorCuenta[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                    cbCuentas.addItem(Principal.vectorCuenta[i].getNoCuenta());
                }
            }
        }

        JLabel lblSaldo = new JLabel();
        cbCuentas.addActionListener(e -> lblSaldo.setText("Saldo disponible: " + Principal.vectorCuenta[Integer.parseInt(cbCuentas.getSelectedItem().toString())].getMontoFinal()));


        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblCuentas)
                        .addComponent(cbCuentas)
                        .addComponent(lblSaldo)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblCuentas)
                        .addComponent(cbCuentas)
                        .addComponent(lblSaldo)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();

    }
    //endregion

    //region cambioCheques
    private void cambioCheques(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            selectionPanel(selection);
        });

        JLabel lblCuenta = new JLabel("Cta. Retiro");
        JTextField txtCuenta = new JTextField();
        txtCuenta.setMaximumSize(new Dimension(getWidth(), 40));

        JLabel lblDeposito = new JLabel("Cta. Deposito");
        JComboBox cbCuentas = new JComboBox();
        cbCuentas.setMaximumSize(new Dimension(getWidth(), 40));
        for (int i = 0; i < Principal.vectorCuenta.length; i++) {
            if (Principal.vectorCuenta[i] != null) {
                if (Principal.vectorCuenta[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                    cbCuentas.addItem(Principal.vectorCuenta[i].getNoCuenta());
                }
            }
        }

        JLabel lblMonto = new JLabel("Monto");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));

        JLabel lblUbicacion = new JLabel("Ubicacion");
        cbUbicacion = new JComboBox();
        cbUbicacion.setMaximumSize(new Dimension(getWidth(), 40));
        cbUbicacion = Utility.buildCombo(cbUbicacion, selection);

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Procesar", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            boolean successful = false;
            for (int i = 0; i < Principal.vectorCuenta.length; i++) {
                if (Principal.vectorCuenta[i] != null) {
                    if (Principal.vectorCuenta[i].getNoCuenta() == Integer.parseInt(txtCuenta.getText())
                            && Integer.parseInt(txtCuenta.getText()) != Integer.parseInt(cbCuentas.getSelectedItem().toString())) {
                        for (int o = 0; o < Principal.vectorCuenta.length; o++) {
                            if (Principal.vectorCuenta[o] != null) {
                                if (Principal.vectorCuenta[o].getNoCuenta() == Integer.parseInt(cbCuentas.getSelectedItem().toString())) {
                                    Principal.vectorCuenta[i].setMontoFinal(Principal.vectorCuenta[i].getMontoFinal() - Double.parseDouble(txtMonto.getText()));
                                    Principal.vectorCuenta[o].setMontoFinal(Principal.vectorCuenta[o].getMontoFinal() + Double.parseDouble(txtMonto.getText()));
                                    Transaccion transaction = new Transaccion();
                                    transaction.setId(Principal.contadorTransaccion);
                                    transaction.setCliente(Principal.clienteChoose);
                                    transaction.setFechaTransaccion(new Date());
                                    transaction.setMonto(Double.parseDouble(txtMonto.getText()));
                                    transaction.setTipoOperacion("CambioCheque");
                                    transaction.setCuentaRetiro(Principal.vectorCuenta[i].getNoCuenta());
                                    transaction.setCuentaDestino(Principal.vectorCuenta[o].getNoCuenta());
                                    Utility.searchAndSetLocation(cbUbicacion.getSelectedItem().toString(), selection, transaction);
                                    Principal.vectorTransaccion[Principal.contadorTransaccion] = transaction;
                                    successful = true;
                                    Principal.contadorTransaccion++;
                                }
                            }
                        }

                    }
                }

            }
            messageConfirmation(successful);
            txtCuenta.setText("");
            txtMonto.setText("");
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblCuenta)
                        .addComponent(txtCuenta)
                        .addComponent(lblDeposito)
                        .addComponent(cbCuentas)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblCuenta)
                        .addComponent(txtCuenta)
                        .addComponent(lblDeposito)
                        .addComponent(cbCuentas)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    //region PagoServicios
    private void pagoServiciosPanel(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            selectionPanel(selection);
        });

        JLabel lblServicio = new JLabel("Servicio a pagar");
        JComboBox cbServicio = new JComboBox();
        cbServicio.setMaximumSize(new Dimension(getWidth(), 40));
        cbServicio.addItem("Agua");
        cbServicio.addItem("Luz");
        cbServicio.addItem("Telefono");

        JLabel lblMonto = new JLabel("Monto a pagar");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));

        JLabel lblTipo = new JLabel("Tipo Pago");
        JComboBox cbTipo = new JComboBox();
        cbTipo.setMaximumSize(new Dimension(getWidth(), 40));
        cbTipo.addItem("Efectivo");
        cbTipo.addItem("Cheque");

        JLabel lblUbicacion = new JLabel("Ubicacion");
        cbUbicacion = new JComboBox();
        cbUbicacion.setMaximumSize(new Dimension(getWidth(), 40));
        cbUbicacion = Utility.buildCombo(cbUbicacion, selection);

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Procesar", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            boolean successful = false;

            Transaccion transaction = new Transaccion();
            transaction.setId(Principal.contadorTransaccion);
            transaction.setCliente(Principal.clienteChoose);
            transaction.setFechaTransaccion(new Date());
            transaction.setMonto(Double.parseDouble(txtMonto.getText()));
            if (cbTipo.getSelectedItem().equals("Efectivo"))
                transaction.setTipo(true);
            else {
                for (int i = 0; i < Principal.vectorCuenta.length; i++) {
                    if (Principal.vectorCuenta[i] != null) {
                        if (Principal.vectorCuenta[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                            System.out.println(Principal.vectorCuenta[i].getMontoFinal());
                            Principal.vectorCuenta[i].setMontoFinal(Principal.vectorCuenta[i].getMontoFinal() - Double.parseDouble(txtMonto.getText()));
                            System.out.println(Principal.vectorCuenta[i].getMontoFinal());
                        }
                    }
                }
                transaction.setTipo(false);
            }
            transaction.setTipoOperacion("PagoServicio");
            switch (selection) {
                case 1:
                    for (int o = 0; o < Principal.vectorAgencia.length; o++) {
                        if (Principal.vectorAgencia[o] != null) {
                            if (Principal.vectorAgencia[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                Principal.vectorAgencia[o].setEfectivo(Principal.vectorAgencia[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                            }
                        }
                    }
                    break;

                case 2:
                    for (int o = 0; o < Principal.vectorAutobanco.length; o++) {
                        if (Principal.vectorAutobanco[o] != null) {
                            if (Principal.vectorAutobanco[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                Principal.vectorAutobanco[o].setEfectivo(Principal.vectorAutobanco[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                            }
                        }
                    }
                    break;

            }
            Utility.searchAndSetLocation(cbUbicacion.getSelectedItem().toString(), selection, transaction);
            transaction.setServicio(cbServicio.getSelectedItem().toString());
            Principal.vectorTransaccion[Principal.contadorTransaccion] = transaction;
            successful = true;
            Principal.contadorTransaccion++;

            messageConfirmation(successful);
            cbServicio.setSelectedItem("Agua");
            txtMonto.setText("");
            cbTipo.setSelectedItem("Efectivo");
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblServicio)
                        .addComponent(cbServicio)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblServicio)
                        .addComponent(cbServicio)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    //region PagoTarjeta
    private void pagoTarjetaPanel(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            selectionPanel(selection);
        });

        JLabel lblTarjeta = new JLabel("No. Tarjeta");
        JComboBox cbTarjeta = new JComboBox();
        cbTarjeta.setMaximumSize(new Dimension(getWidth(), 40));
        for (int i = 0; i < Principal.vectorTarjeta.length; i++) {
            if (Principal.vectorTarjeta[i] != null) {
                if (Principal.vectorTarjeta[i].getCliente().getId() == Principal.clienteChoose.getId() &&
                        Principal.vectorTarjeta[i].isEstado()) {
                    cbTarjeta.addItem(Integer.toString(Principal.vectorTarjeta[i].getNoTarjeta()));
                } else {
                    messageConfirmation(false);
                }
            }
        }


        JLabel lblMonto = new JLabel("Monto a pagar");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));
        cbTarjeta.addActionListener(e -> txtMonto.setText("" + Principal.vectorTarjeta[Integer.parseInt(cbTarjeta.getSelectedItem().toString())].getMontoAdeudado()));

        JLabel lblTipo = new JLabel("Tipo Pago");
        JComboBox cbTipo = new JComboBox();
        cbTipo.setMaximumSize(new Dimension(getWidth(), 40));
        cbTipo.addItem("Efectivo");
        cbTipo.addItem("Cheque");

        JLabel lblUbicacion = new JLabel("Ubicacion");
        cbUbicacion = new JComboBox();
        cbUbicacion.setMaximumSize(new Dimension(getWidth(), 40));
        cbUbicacion = Utility.buildCombo(cbUbicacion, selection);

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Procesar", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            boolean successful = false;

            Transaccion transaction = new Transaccion();
            transaction.setId(Principal.contadorTransaccion);
            transaction.setCliente(Principal.clienteChoose);
            transaction.setFechaTransaccion(new Date());
            transaction.setMonto(Double.parseDouble(txtMonto.getText()));
            if (cbTipo.getSelectedItem().equals("Efectivo"))
                transaction.setTipo(true);
            else {
                for (int i = 0; i < Principal.vectorCuenta.length; i++) {
                    if (Principal.vectorCuenta[i] != null) {
                        if (Principal.vectorCuenta[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                            System.out.println(Principal.vectorCuenta[i].getMontoFinal());
                            Principal.vectorCuenta[i].setMontoFinal(Principal.vectorCuenta[i].getMontoFinal() - Double.parseDouble(txtMonto.getText()));
                            System.out.println(Principal.vectorCuenta[i].getMontoFinal());
                        }
                    }
                }
                transaction.setTipo(false);
            }
            for (int i = 0; i < Principal.vectorTarjeta.length; i++) {
                if (Principal.vectorTarjeta[i] != null) {
                    if (Principal.vectorTarjeta[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                        Principal.vectorTarjeta[i].setMontoAdeudado(Principal.vectorTarjeta[i].getMontoAdeudado() - Double.parseDouble(txtMonto.getText()));
                    }
                }
            }
            transaction.setTipoOperacion("PagoTarjeta");
            transaction.setNoTarjeta(Integer.parseInt(cbTarjeta.getSelectedItem().toString()));
            switch (selection) {
                case 1:
                    for (int o = 0; o < Principal.vectorAgencia.length; o++) {
                        if (Principal.vectorAgencia[o] != null) {
                            if (Principal.vectorAgencia[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                Principal.vectorAgencia[o].setEfectivo(Principal.vectorAgencia[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                            }
                        }
                    }
                    break;

                case 2:
                    for (int o = 0; o < Principal.vectorAutobanco.length; o++) {
                        if (Principal.vectorAutobanco[o] != null) {
                            if (Principal.vectorAutobanco[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                Principal.vectorAutobanco[o].setEfectivo(Principal.vectorAutobanco[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                            }
                        }
                    }
                    break;

            }
            Utility.searchAndSetLocation(cbUbicacion.getSelectedItem().toString(), selection, transaction);
            Principal.vectorTransaccion[Principal.contadorTransaccion] = transaction;
            successful = true;
            Principal.contadorTransaccion++;

            messageConfirmation(successful);

            txtMonto.setText("");
            cbTipo.setSelectedItem("Efectivo");
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblTarjeta)
                        .addComponent(cbTarjeta)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblTarjeta)
                        .addComponent(cbTarjeta)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    //region pagoPrestamo
    private void pagoPrestamoPanel(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> selectionPanel(selection));

        JLabel lblPrestamo = new JLabel("No. Prestamo");
        JComboBox cbPrestamo = new JComboBox();
        cbPrestamo.setMaximumSize(new Dimension(getWidth(), 40));
        for (int i = 0; i < Principal.vectorPrestamo.length; i++) {
            if (Principal.vectorPrestamo[i] != null) {
                if (Principal.vectorPrestamo[i].getCliente().getId() == Principal.clienteChoose.getId() &&
                        Principal.vectorPrestamo[i].isEstado()) {
                    cbPrestamo.addItem(Integer.toString(Principal.vectorPrestamo[i].getNoPrestamo()));
                } else {
                    messageConfirmation(false);
                }
            }
        }

        JLabel lblMonto = new JLabel("Monto a pagar");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));
        cbPrestamo.addActionListener(e -> txtMonto.setText("" + Principal.vectorPrestamo[Integer.parseInt(cbPrestamo.getSelectedItem().toString())].getMontoAdeudado()));

        JLabel lblTipo = new JLabel("Tipo Pago");
        JComboBox cbTipo = new JComboBox();
        cbTipo.setMaximumSize(new Dimension(getWidth(), 40));
        cbTipo.addItem("Efectivo");
        cbTipo.addItem("Cheque");

        JLabel lblUbicacion = new JLabel("Ubicacion");
        cbUbicacion = new JComboBox();
        cbUbicacion.setMaximumSize(new Dimension(getWidth(), 40));
        cbUbicacion = Utility.buildCombo(cbUbicacion, selection);

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Procesar", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            boolean successful = false;

            Transaccion transaction = new Transaccion();
            transaction.setId(Principal.contadorTransaccion);
            transaction.setCliente(Principal.clienteChoose);
            transaction.setFechaTransaccion(new Date());
            transaction.setMonto(Double.parseDouble(txtMonto.getText()));
            if (cbTipo.getSelectedItem().equals("Efectivo"))
                transaction.setTipo(true);
            else {
                for (int i = 0; i < Principal.vectorCuenta.length; i++) {
                    if (Principal.vectorCuenta[i] != null) {
                        if (Principal.vectorCuenta[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                            System.out.println(Principal.vectorCuenta[i].getMontoFinal());
                            Principal.vectorCuenta[i].setMontoFinal(Principal.vectorCuenta[i].getMontoFinal() - Double.parseDouble(txtMonto.getText()));
                            System.out.println(Principal.vectorCuenta[i].getMontoFinal());
                        }
                    }
                }
                transaction.setTipo(false);
            }
            for (int i = 0; i < Principal.vectorPrestamo.length; i++) {
                if (Principal.vectorPrestamo[i] != null) {
                    if (Principal.vectorPrestamo[i].getCliente().getId() == Principal.clienteChoose.getId()) {
                        Principal.vectorPrestamo[i].setMontoAdeudado(Principal.vectorPrestamo[i].getMontoAdeudado() - Double.parseDouble(txtMonto.getText()));
                    }
                }
            }
            transaction.setTipoOperacion("PagoPrestamo");
            switch (selection) {
                case 1:
                    for (int o = 0; o < Principal.vectorAgencia.length; o++) {
                        if (Principal.vectorAgencia[o] != null) {
                            if (Principal.vectorAgencia[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                Principal.vectorAgencia[o].setEfectivo(Principal.vectorAgencia[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                            }
                        }
                    }
                    break;

                case 2:
                    for (int o = 0; o < Principal.vectorAutobanco.length; o++) {
                        if (Principal.vectorAutobanco[o] != null) {
                            if (Principal.vectorAutobanco[o].getNombre() == cbUbicacion.getSelectedItem().toString()) {
                                Principal.vectorAutobanco[o].setEfectivo(Principal.vectorAutobanco[o].getEfectivo() + Double.parseDouble(txtMonto.getText()));
                            }
                        }
                    }
                    break;

            }
            Utility.searchAndSetLocation(cbUbicacion.getSelectedItem().toString(), selection, transaction);
            transaction.setNoPrestamo(Integer.parseInt(cbPrestamo.getSelectedItem().toString()));
            Principal.vectorTransaccion[Principal.contadorTransaccion] = transaction;
            successful = true;
            Principal.contadorTransaccion++;

            messageConfirmation(successful);

            txtMonto.setText("");
            cbTipo.setSelectedItem("Efectivo");
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblPrestamo)
                        .addComponent(cbPrestamo)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblPrestamo)
                        .addComponent(cbPrestamo)
                        .addComponent(lblMonto)
                        .addComponent(txtMonto)
                        .addComponent(lblTipo)
                        .addComponent(cbTipo)
                        .addComponent(lblUbicacion)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    //region solicitarTarjeta
    private void solicitarTarjeta(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> selectionPanel(selection));

        JLabel lblSolicitar = new JLabel("Desea enviar la solicitud?");
        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Enviar solicitud de tarjeta", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            TarjetaCredito tarjetaCredito = new TarjetaCredito();
            tarjetaCredito.setNoTarjeta(Principal.contadorTarjeta);
            tarjetaCredito.setCliente(Principal.clienteChoose);
            tarjetaCredito.setLimiteCredito(5000);
            tarjetaCredito.setMontoAdeudado(0);
            tarjetaCredito.setNoCompras(0);
            tarjetaCredito.setEstado(false);
            tarjetaCredito.setEspera(true);
            Principal.vectorTarjeta[Principal.contadorTarjeta] = tarjetaCredito;
            Principal.contadorTarjeta++;
            messageConfirmation(true);
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblSolicitar)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblSolicitar)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    //region solicitarPrestamo
    private void solicitarPrestamo(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> selectionPanel(selection));

        JLabel lblSolicitar = new JLabel("Cuanto deseas solicitar?");
        JTextField txtMonto = new JTextField();
        txtMonto.setMaximumSize(new Dimension(getWidth(), 40));

        cbUbicacion = new JComboBox();
        cbUbicacion.setMaximumSize(new Dimension(getWidth(), 40));
        cbUbicacion = Utility.buildCombo(cbUbicacion, selection);

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Enviar solicitud de prestamo", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            Prestamo prestamo = new Prestamo();
            prestamo.setNoPrestamo(Principal.contadorPrestamo);
            prestamo.setCliente(Principal.clienteChoose);
            prestamo.setFechaPrestamo(new Date());
            prestamo.setMontoPrestado(Double.parseDouble(txtMonto.getText()));
            prestamo.setMontoAdeudado(Double.parseDouble(txtMonto.getText()));
            prestamo.setEspera(true);
            Principal.vectorPrestamo[Principal.contadorPrestamo] = prestamo;
            Principal.contadorPrestamo++;
            messageConfirmation(true);
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblSolicitar)
                        .addComponent(txtMonto)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblSolicitar)
                        .addComponent(txtMonto)
                        .addComponent(cbUbicacion)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();
    }
    //endregion

    private void simularCompra(int selection) {
        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> selectionPanel(selection));

        JLabel lblCompra = new JLabel("Selecciona el articulo a comprar");
        JComboBox cbCompra = new JComboBox();
        cbCompra.setMaximumSize(new Dimension(getWidth(), 40));
        cbCompra.addItem("Selecciona...");
        cbCompra.addItem("Cereal");
        cbCompra.addItem("Azucar");
        cbCompra.addItem("Gaseosa");
        cbCompra.addItem("Cafe");

        JLabel lblPrecio = new JLabel("Precios");
        cbCompra.addActionListener(e -> {
            switch (cbCompra.getSelectedItem().toString()) {
                case "Cereal":
                case "Gaseosa":
                    lblPrecio.setText("Q 15");
                    break;

                case "Azucar":
                    lblPrecio.setText("Q 12");
                    break;

                case "Cafe":
                    lblPrecio.setText("Q 40");
                    break;
            }

        });

        btnConfirmar = new JButton();
        btnConfirmar = Utility.lateralButtonProperties(btnConfirmar, "Comprar con tarjeta", this);
        btnConfirmar.setBackground(Color.decode("#84de00"));
        btnConfirmar.addActionListener(e -> {
            for (int i = 0; i < Principal.vectorTarjeta.length; i++) {
                if (Principal.vectorTarjeta[i] != null){
                    if (Principal.vectorTarjeta[i].getCliente().getId() == Principal.clienteChoose.getId()){
                        String[] getNumber = lblPrecio.getText().split(" ");
                        int precio = Integer.parseInt(getNumber[1]);
                        Principal.vectorTarjeta[i].setMontoAdeudado(Principal.vectorTarjeta[i].getMontoAdeudado() + precio);
                        Principal.vectorTarjeta[i].setNoCompras(Principal.vectorTarjeta[i].getNoCompras() + 1);
                        messageConfirmation(true);
                    }
                }
            }
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(lblCompra)
                        .addComponent(cbCompra)
                        .addComponent(lblPrecio)
                        .addComponent(btnConfirmar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(lblCompra)
                        .addComponent(cbCompra)
                        .addComponent(lblPrecio)
                        .addComponent(btnConfirmar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();


    }

    private void messageConfirmation(boolean successful) {
        if (successful) {
            JOptionPane.showMessageDialog(this,
                    "Transaccion Exitosa",
                    "Transaccion",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this,
                    "Transaccion fallo",
                    "Transaccion",
                    JOptionPane.WARNING_MESSAGE);
        }
    }

}
