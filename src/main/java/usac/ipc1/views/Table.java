package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.*;
import usac.ipc1.models.*;

import javax.swing.*;

public class Table extends JTable {
    private Cliente vectorCliente[];
    private Agencia vectorAgencia[];
    private Autobanco vectorAutobanco[];
    private Cajero vectorCajero[];
    private Empleado vectorEmpleado[];
    private TarjetaCredito vectorTarjeta[];
    private Prestamo vectorPrestamo[];
    public enum TipoTabla { CLIENTE, AGENCIA, AUTOBANCO, CAJERO, EMPLEADO, TARJETA, PRESTAMO }
    private TipoTabla tipoTabla;

    public Table(){
        super();
    }

    public Table(Object vector[], TipoTabla tipoTabla){
        super();
        switch (tipoTabla){
            case CLIENTE:
                this.vectorCliente = (Cliente[]) vector;
                setModel(new TablaClientes(vectorCliente));
                break;

            case AGENCIA:
                this.vectorAgencia = (Agencia[]) vector;
                setModel(new TablaAgencia(vectorAgencia));
                break;

            case AUTOBANCO:
                this.vectorAutobanco = (Autobanco[]) vector;
                setModel(new TablaAutobanco(vectorAutobanco));
                break;

            case CAJERO:
                this.vectorCajero = (Cajero[]) vector;
                setModel(new TablaCajeros(vectorCajero));
                break;

            case EMPLEADO:
                this.vectorEmpleado = (Empleado[]) vector;
                setModel(new TablaEmpleado(vectorEmpleado));
                break;

            case TARJETA:
                this.vectorTarjeta = (TarjetaCredito[]) vector;
                setModel(new TablaTarjeta(vectorTarjeta));
                break;

            case PRESTAMO:
                this.vectorPrestamo = (Prestamo[]) vector;
                setModel(new TablaPrestamo(vectorPrestamo));
                break;
        }
        this.tipoTabla = tipoTabla;
    }

    //region cliente

    public void agregar(Cliente cliente){
        vectorCliente[Principal.contadorClientes] = cliente;
        Principal.vectorCliente[Principal.contadorClientes] = cliente;
        Principal.contadorClientes++;
        revalidate();
        repaint();
    }

    public void editar(Cliente cliente, int position){
        vectorCliente[position] = cliente;
        Principal.vectorCliente[position] = cliente;
        revalidate();
        repaint();
    }

    //endregion

    //region Agencia
    public void agregar(Agencia agencia){
        vectorAgencia[Principal.contadorAgencia] = agencia;
        Principal.vectorAgencia[Principal.contadorAgencia] = agencia;
        Principal.contadorAgencia++;
        revalidate();
        repaint();
    }

    public void editar(Agencia agencia, int position){
        vectorAgencia[position] = agencia;
        Principal.vectorAgencia[position] = agencia;
        revalidate();
        repaint();
    }
    //endregion

    //region Autobanco
    public void agregar(Autobanco autobanco){
        vectorAutobanco[Principal.contadorAutobanco] = autobanco;
        Principal.vectorAutobanco[Principal.contadorAutobanco] = autobanco;
        Principal.contadorAutobanco++;
        revalidate();
        repaint();
    }

    public void editar(Autobanco autobanco, int position){
        vectorAutobanco[position] = autobanco;
        Principal.vectorAutobanco[position] = autobanco;
        revalidate();
        repaint();
    }
    //endregion

    //region Cajero
    public void agregar(Cajero cajero){
        vectorCajero[Principal.contadorCajero] = cajero;
        Principal.vectorCajero[Principal.contadorCajero] = cajero;
        Principal.contadorCajero++;
        revalidate();
        repaint();
    }

    public void editar(Cajero cajero, int position){
        vectorCajero[position] = cajero;
        Principal.vectorCajero[position] = cajero;
        revalidate();
        repaint();
    }
    //endregion

    //region Empleado
    public void agregar(Empleado empleado){
        vectorEmpleado[Principal.contadorEmpleado] = empleado;
        Principal.vectorEmpleado[Principal.contadorEmpleado] = empleado;
        Principal.contadorEmpleado++;
        revalidate();
        repaint();
    }

    public void editar(Empleado empleado, int position){
        vectorEmpleado[position] = empleado;
        Principal.vectorEmpleado[position] = empleado;
        revalidate();
        repaint();
    }
    //endregion

    //region Tarjeta
    public void aprobar(int position, TipoTabla tipoTabla){
        if (tipoTabla == TipoTabla.TARJETA){
            vectorTarjeta[position].setEspera(false);
            vectorTarjeta[position].setEstado(true);
        }else{
            vectorPrestamo[position].setEspera(false);
            vectorPrestamo[position].setEstado(true);
        }
        revalidate();
        repaint();
    }

    public void denegar(int position, TipoTabla tipoTabla){
        if (tipoTabla == TipoTabla.TARJETA){
            vectorTarjeta[position].setEspera(false);
            vectorTarjeta[position].setEstado(false);
        }else{
            vectorPrestamo[position].setEspera(false);
            vectorPrestamo[position].setEstado(false);
        }
        revalidate();
        repaint();
    }
    //endregion

    public void eliminar(int position, TipoTabla tipoTabla){
        switch (tipoTabla){
            case CLIENTE:
                vectorCliente[position] = null;
                Principal.vectorCliente[position] = null;
                break;

            case AGENCIA:
                vectorAgencia[position] = null;
                Principal.vectorAgencia[position] = null;
                break;

            case AUTOBANCO:
                vectorAutobanco[position] = null;
                Principal.vectorAutobanco[position] = null;
                break;

            case CAJERO:
                vectorCajero[position] = null;
                Principal.vectorCajero[position] = null;
                break;

            case EMPLEADO:
                vectorEmpleado[position] = null;
                Principal.vectorCajero[position] = null;
                break;
        }
        revalidate();
        repaint();
    }
}
