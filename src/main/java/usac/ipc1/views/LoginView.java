package usac.ipc1.views;

import javax.swing.*;
import java.awt.*;

public class LoginView extends JFrame {

    private JPanel container;

    public LoginView() {
        super();
        setBounds(new Rectangle(500, 500));
        setTitle("Iniciar sesión");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.PAGE_AXIS));

        JLabel lblUsername = new JLabel("Nombre de usuario");
        lblUsername.setMaximumSize(new Dimension(200, 35));
        lblUsername.setAlignmentX(CENTER_ALIGNMENT);
        lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
        lblUsername.setForeground(Color.decode("#FFFFFF"));

        JTextField txtUsername = new JTextField(10);
        txtUsername.setMaximumSize(new Dimension(200, 35));
        txtUsername.setAlignmentX(CENTER_ALIGNMENT);

        JLabel lblPassword = new JLabel("Contraseña");
        lblPassword.setMaximumSize(new Dimension(200, 35));
        lblPassword.setAlignmentX(CENTER_ALIGNMENT);
        lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
        lblPassword.setForeground(Color.decode("#FFFFFF"));

        JPasswordField txtPassword = new JPasswordField(10);
        txtPassword.setMaximumSize(new Dimension(200, 35));
        txtPassword.setAlignmentX(CENTER_ALIGNMENT);

        JButton btnSubmit = new JButton("Iniciar sesión");
        btnSubmit.setMaximumSize(new Dimension(200, 35));
        btnSubmit.setAlignmentX(CENTER_ALIGNMENT);

        JButton btnTransactions = new JButton("Transacciones");
        btnTransactions.setMaximumSize(new Dimension(200, 35));
        btnTransactions.setAlignmentX(CENTER_ALIGNMENT);

        btnSubmit.addActionListener(e -> { // Usando lambda
            if (txtUsername.getText().equals("admin") && txtPassword.getText().equals("123456")) {
                dispose();
                new AdminView();
            } else
                JOptionPane.showMessageDialog(null, "Credenciales invalidas");
        });

        btnTransactions.addActionListener(e -> {
            dispose();
            new ClienteChooseView();
        });

        // container.add(lblUsername);
        container.add(Box.createVerticalGlue());
        container.add(lblUsername);
        container.add(txtUsername);
        container.add(Box.createRigidArea(new Dimension(0, 10)));
        container.add(lblPassword);
        container.add(txtPassword);
        container.add(Box.createRigidArea(new Dimension(0, 10)));
        container.add(btnSubmit);
        container.add(Box.createRigidArea(new Dimension(0, 10)));
        container.add(btnTransactions);
        container.add(Box.createVerticalGlue());

        container.setBackground(Color.decode("#407F7F"));


        add(container);
        setVisible(true);

    }

}
