package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.utils.Utility;

import javax.swing.*;
import java.awt.*;

public class ClienteChooseView extends JFrame {

    private JPanel principalContainer;
    private JButton btnChoose; // Seleccionar cliente
    private JButton btnAgencia, btnAutobanco, btnCajero, btnCallcenter, btnCreateAccount, btnComprar; // Seleccionar destino

    public ClienteChooseView() {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(800, 500));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Transacciones BDE");

        panelPrincipal(Principal.vectorCliente, Table.TipoTabla.CLIENTE);

        setVisible(true);
    }

    private void panelPrincipal(Object vector[], Table.TipoTabla tipoTabla) {

        Table tabla = new Table(vector, tipoTabla);
        JScrollPane scrollPane = new JScrollPane(tabla);
        scrollPane.setMaximumSize(new Dimension(1500, 184));

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Cerrar sesión");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            if (Principal.clienteChoose != null)
                Principal.clienteChoose = null;
            dispose();
            new LoginView();
        });

        btnChoose = new JButton();
        btnChoose = Utility.lateralButtonProperties(btnChoose, "Seleccionar", ClienteChooseView.this);
        btnChoose.setBackground(Color.decode("#99cc33"));
        btnChoose.addActionListener(e -> {
            if (tabla.getSelectedRowCount() > 0) {
                Principal.clienteChoose = Principal.vectorCliente[tabla.getSelectedRow()];
                selectBank();
            } else {
                JOptionPane.showMessageDialog(ClienteChooseView.this,
                        "Debe seleccionar un cliente",
                        "Seleccionar cliente",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        JPanel buttonsPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(buttonsPanel, BoxLayout.X_AXIS);
        buttonsPanel.setLayout(boxLayout);
        buttonsPanel.add(btnChoose);

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(scrollPane)
                        .addComponent(buttonsPanel)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(scrollPane)
                        .addComponent(buttonsPanel)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();

        if (Principal.clienteChoose != null) {
            selectBank();
        }
    }

    private void selectBank() {

        if (principalContainer != null) {
            remove(principalContainer);
        }

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Cerrar sesión");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            if (Principal.clienteChoose != null)
                Principal.clienteChoose = null;
            dispose();
            new LoginView();
        });

        btnAgencia = new JButton();
        btnAgencia = Utility.lateralButtonProperties(btnAgencia, "Agencia", ClienteChooseView.this);
        btnAgencia.setBackground(Color.decode("#2ea2de"));
        btnAgencia.addActionListener(e -> {
            dispose();
            new OperacionesView(1);
        });

        btnAutobanco = new JButton();
        btnAutobanco = Utility.lateralButtonProperties(btnAutobanco, "Autobanco", ClienteChooseView.this);
        btnAutobanco.setBackground(Color.decode("#ed7f01"));
        btnAutobanco.addActionListener(e -> {
            dispose();
            new OperacionesView(2);
        });

        btnCajero = new JButton();
        btnCajero = Utility.lateralButtonProperties(btnCajero, "Cajero", ClienteChooseView.this);
        btnCajero.setBackground(Color.decode("#bb12ec"));
        btnCajero.addActionListener(e -> {
            dispose();
            new OperacionesView(3);
        });

        btnCallcenter = new JButton();
        btnCallcenter = Utility.lateralButtonProperties(btnCallcenter, "Call Center", ClienteChooseView.this);
        btnCallcenter.setBackground(Color.decode("#84de00"));
        btnCallcenter.addActionListener(e -> {
            dispose();
            new OperacionesView(4);
        });

        btnCreateAccount = new JButton();
        btnCreateAccount = Utility.lateralButtonProperties(btnCreateAccount, "Crear cuenta", ClienteChooseView.this);
        btnCreateAccount.addActionListener(e -> {
            dispose();
            new OperacionesView(Principal.clienteChoose, this);
        });

        btnComprar = new JButton();
        btnComprar = Utility.lateralButtonProperties(btnComprar, "Realizar compra", ClienteChooseView.this);
        btnComprar.setBackground(Color.decode("#2ea2de"));
        btnComprar.addActionListener(e -> {
            dispose();
            new OperacionesView(100);
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(btnAgencia)
                        .addComponent(btnAutobanco)
                        .addComponent(btnCajero)
                        .addComponent(btnCallcenter)
                        .addComponent(btnCreateAccount)
                        .addComponent(btnComprar)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(btnAgencia)
                        .addComponent(btnAutobanco)
                        .addComponent(btnCajero)
                        .addComponent(btnCallcenter)
                        .addComponent(btnCreateAccount)
                        .addComponent(btnComprar)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();

    }

}
