package usac.ipc1.models;

import usac.ipc1.beans.TarjetaCredito;

import javax.swing.table.AbstractTableModel;

public class TablaTarjeta extends AbstractTableModel {

    private TarjetaCredito vectorTarjeta[];
    private final String[] columnName = new String[]{"No.", "Cliente", "Estado"};

    public TablaTarjeta(TarjetaCredito[] vectorTarjeta) {
        this.vectorTarjeta = vectorTarjeta;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorTarjeta.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        TarjetaCredito tarjetaCredito = this.vectorTarjeta[rowIndex];
        if (tarjetaCredito != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(tarjetaCredito.getNoTarjeta());
                    break;

                case 1:
                    value = tarjetaCredito.getCliente().getNombre();
                    break;

                case 2:
                    if (tarjetaCredito.isEspera())
                        value = "En espera";
                    else{
                        if (tarjetaCredito.isEstado())
                            value = "Aprobada";
                        else
                            value = "Denegada";
                    }

                    break;
            }
        }

        return value;
    }
}
