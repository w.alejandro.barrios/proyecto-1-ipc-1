package usac.ipc1.models;

import usac.ipc1.beans.Cajero;

import javax.swing.table.AbstractTableModel;

public class TablaCajeros extends AbstractTableModel {

    private Cajero vectorCajero[];
    private final String[] columnName = new String[]{"Id.", "Ubicacion", "Efectivo", "Estado", "Transacciones"};

    public TablaCajeros(Cajero[] vectorCajero){
        this.vectorCajero = vectorCajero;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorCajero.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Cajero cajero = this.vectorCajero[rowIndex];
        if (cajero != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(cajero.getId());
                    break;

                case 1:
                    value = cajero.getUbicacion();
                    break;

                case 2:
                    value = Double.toString(cajero.getEfectivo());
                    break;

                case 3:
                    if(cajero.isEstado())
                        value = "Activo";
                    else
                        value = "Mantenimiento";
                    break;

                case 4:
                    value = Integer.toString(cajero.getNoTransaccionesProcesadas());
                    break;
            }
        }
        return value;
    }
}
