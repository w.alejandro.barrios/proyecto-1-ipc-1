package usac.ipc1.models;

import usac.ipc1.beans.Agencia;

import javax.swing.table.AbstractTableModel;

public class TablaAgencia extends AbstractTableModel {

    private Agencia vectorAgencia[];
    private final String[] columnName = new String[]{"Id.", "Nombre", "Direccion", "Telefono", "Cajas", "Escritorios", "Efectivo"};

    public TablaAgencia(Agencia[] vectorAgencia){
        this.vectorAgencia = vectorAgencia;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorAgencia.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Agencia agencia = this.vectorAgencia[rowIndex];
        if (agencia != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(agencia.getId());
                    break;

                case 1:
                    value = agencia.getNombre();
                    break;

                case 2:
                    value = agencia.getDireccion();
                    break;

                case 3:
                    value = Integer.toString(agencia.getTelefono());
                    break;

                case 4:
                    value = Integer.toString(agencia.getNoCajas());
                    break;

                case 5:
                    value = Integer.toString(agencia.getNoEscritorios());
                    break;

                case 6:
                    value = Double.toString(agencia.getEfectivo());
                    break;
            }
        }

        return value;
    }

}
