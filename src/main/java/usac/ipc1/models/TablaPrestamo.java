package usac.ipc1.models;

import usac.ipc1.beans.Prestamo;

import javax.swing.table.AbstractTableModel;

public class TablaPrestamo extends AbstractTableModel {

    private Prestamo vectorPrestamo[];
    private final String[] columnName = new String[]{"No.", "Cliente", "Monto", "Estado"};

    public TablaPrestamo(Prestamo[] vectorPrestamo) {
        this.vectorPrestamo = vectorPrestamo;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorPrestamo.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Prestamo prestamo = this.vectorPrestamo[rowIndex];
        if (prestamo != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(prestamo.getNoPrestamo());
                    break;

                case 1:
                    value = prestamo.getCliente().getNombre();
                    break;

                case 2:
                    value = "Q"+prestamo.getMontoPrestado();
                    break;

                case 3:
                    if (prestamo.isEspera())
                        value = "En espera";
                    else{
                        if (prestamo.isEstado())
                            value = "Aprobada";
                        else
                            value = "Denegada";
                    }

                    break;
            }
        }

        return value;
    }
}
