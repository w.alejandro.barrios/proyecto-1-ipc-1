package usac.ipc1.models;

import usac.ipc1.beans.Autobanco;

import javax.swing.table.AbstractTableModel;

public class TablaAutobanco extends AbstractTableModel {

    private Autobanco vectorAutobanco[];
    private final String[] columnName = new String[]{"Id.", "Nombre", "Direccion", "Telefono", "Cajas", "Escritorios", "Efectivo", "Auto cajas"};

    public TablaAutobanco(Autobanco[] vectorAutobanco){
        this.vectorAutobanco = vectorAutobanco;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorAutobanco.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Autobanco autobanco = this.vectorAutobanco[rowIndex];
        if (autobanco != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(autobanco.getId());
                    break;

                case 1:
                    value = autobanco.getNombre();
                    break;

                case 2:
                    value = autobanco.getDireccion();
                    break;

                case 3:
                    value = Integer.toString(autobanco.getTelefono());
                    break;

                case 4:
                    value = Integer.toString(autobanco.getNoCajas());
                    break;

                case 5:
                    value = Integer.toString(autobanco.getNoEscritorios());
                    break;

                case 6:
                    value = Double.toString(autobanco.getEfectivo());
                    break;

                case 7:
                    value = Integer.toString(autobanco.getNoCajasAutobanco());
                    break;
            }
        }

        return value;
    }

}
