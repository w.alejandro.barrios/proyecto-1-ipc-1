package usac.ipc1.models;

import usac.ipc1.beans.Empleado;

import javax.swing.table.AbstractTableModel;

public class TablaEmpleado extends AbstractTableModel {

    private Empleado vectorEmpleado[];
    private final String[] columnName = new String[]{"Id.", "Nombre", "Telefono"};

    public TablaEmpleado(Empleado[] vectorEmpleado) {
        this.vectorEmpleado = vectorEmpleado;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorEmpleado.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Empleado empleado = this.vectorEmpleado[rowIndex];
        if (empleado != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(empleado.getId());
                    break;

                case 1:
                    value = empleado.getNombre();
                    break;

                case 2:
                    value = Integer.toString(empleado.getTelefono());
                    break;
            }
        }

        return value;
    }

}
