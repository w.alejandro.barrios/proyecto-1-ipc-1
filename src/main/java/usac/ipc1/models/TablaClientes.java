package usac.ipc1.models;

import usac.ipc1.beans.Cliente;

import javax.swing.table.AbstractTableModel;

public class TablaClientes extends AbstractTableModel {

    private Cliente vectorCliente[];
    private final String[] columnName = new String[]{"Id.", "Nombre", "Direccion", "Telefono"};

    public TablaClientes(Cliente[] vectorUsuario) {
        this.vectorCliente = vectorUsuario;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorCliente.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Cliente cliente = this.vectorCliente[rowIndex];
        if (cliente != null){
            switch (columnIndex){
                case 0:
                    value = Integer.toString(cliente.getId());
                    break;

                case 1:
                    value = cliente.getNombre();
                    break;

                case 2:
                    value = cliente.getDireccion();
                    break;

                case 3:
                    value = Integer.toString(cliente.getTelefono());
                    break;
            }
        }

        return value;
    }
}
