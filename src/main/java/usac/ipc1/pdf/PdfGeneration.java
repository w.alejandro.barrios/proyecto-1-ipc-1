package usac.ipc1.pdf;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import usac.ipc1.Principal;
import usac.ipc1.beans.Cliente;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

import static com.itextpdf.kernel.xmp.PdfConst.Date;

public class PdfGeneration {

    public static final String DEST = "results/chapter01/Report.pdf";

    public PdfGeneration(int selection) {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        try {
            createPdf(DEST, selection);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public PdfGeneration(Cliente cliente) {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        try {
            createPdfClient(DEST, cliente);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createPdfClient(String destination, Cliente cliente) throws IOException {
        // Initialize PDF
        FileOutputStream fileOutputStream = new FileOutputStream(destination);
        PdfWriter writer = new PdfWriter(fileOutputStream);

        // Initialize pdf document
        PdfDocument pdf = new PdfDocument(writer);

        // Initialize document
        Document document = new Document(pdf);

        float[] pointColumnWidths = {100F, 100F, 100F, 100F};
        Table table = new Table(pointColumnWidths);
        Cell cell1 = new Cell();
        Cell cell2 = new Cell();
        Cell cell3 = new Cell();
        Cell cell4 = new Cell();

        document.add(new Paragraph("INFORMACION DEL CLIENTE: " + cliente.getNombre()));
        document.add(new Paragraph(""));
        document.add(new Paragraph("Nombre y apellido: " + cliente.getNombre() + " " + cliente.getApellido()));
        document.add(new Paragraph("Direccion: " + cliente.getDireccion()));
        document.add(new Paragraph("Telefono: " + cliente.getTelefono()));
        document.add(new Paragraph(""));
        document.add(new Paragraph("CUENTAS"));
        document.add(new Paragraph(""));

        cell1.add("No. Cuenta");
        cell2.add("Aperturada");
        cell3.add("Monto de cta.");
        cell4.add("Tipo");

        for (int i = 0; i < Principal.vectorCuenta.length; i++) {
            if (Principal.vectorCuenta[i] != null) {
                if (Principal.vectorCuenta[i].getCliente().getId() == cliente.getId()) {
                    cell1.add(Integer.toString(Principal.vectorCuenta[i].getNoCuenta()));
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    String newDate = format.format(Principal.vectorCuenta[i].getFechaApertura());
                    cell2.add(newDate);
                    cell3.add(Double.toString(Principal.vectorCuenta[i].getMontoFinal()));
                    if (Principal.vectorCuenta[i].isTipoCuenta())
                        cell4.add("Monetaria");
                    else
                        cell4.add("Ahorro");
                }
            }
        }

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);

        document.add(table);

        // Close document
        document.close();

        File myFile = new File(destination);
        Desktop.getDesktop().open(myFile);
    }

    public void createPdf(String destination, int selection) throws IOException {
        // Initialize PDF
        FileOutputStream fileOutputStream = new FileOutputStream(destination);
        PdfWriter writer = new PdfWriter(fileOutputStream);

        // Initialize pdf document
        PdfDocument pdf = new PdfDocument(writer);

        // Initialize document
        Document document = new Document(pdf);

        // Content

        float[] pointColumnWidths = {100F, 100F, 100F, 100F};
        Table table = new Table(pointColumnWidths);
        Cell cell1 = new Cell();
        Cell cell2 = new Cell();
        Cell cell3 = new Cell();
        Cell cell4 = new Cell();
        switch (selection) {
            case 1: // LISTADO DE AGENCIAS
                document.add(new Paragraph("LISTADO DE AGENCIAS"));
                document.add(new Paragraph(""));

                cell1.add("Nombre");
                cell2.add("Efectivo");
                cell3.add("Direccion");
                cell4.add("Telefono");
                for (int i = 0; i < Principal.vectorAgencia.length; i++) {
                    if (Principal.vectorAgencia[i] != null) {
                        cell1.add(Principal.vectorAgencia[i].getNombre());
                        cell2.add("Q" + Principal.vectorAgencia[i].getEfectivo());
                        cell3.add(Principal.vectorAgencia[i].getDireccion());
                        cell4.add(Integer.toString(Principal.vectorAgencia[i].getTelefono()));
                    }
                }

                for (int i = 0; i < Principal.vectorAutobanco.length; i++) {
                    if (Principal.vectorAutobanco[i] != null) {
                        cell1.add(Principal.vectorAutobanco[i].getNombre());
                        cell2.add("Q" + Principal.vectorAutobanco[i].getEfectivo());
                        cell3.add(Principal.vectorAutobanco[i].getDireccion());
                        cell4.add(Integer.toString(Principal.vectorAutobanco[i].getTelefono()));
                    }
                }
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                break;

            case 2: // LISTADO DE CAJEROS
                document.add(new Paragraph("LISTADO DE CAJEROS"));
                document.add(new Paragraph(""));

                cell1.add("Id.");
                cell2.add("Efectivo");
                cell3.add("Ubicacion");
                cell4.add("Estado");
                for (int i = 0; i < Principal.vectorCajero.length; i++) {
                    if (Principal.vectorCajero[i] != null) {
                        cell1.add(Integer.toString(Principal.vectorCajero[i].getId()));
                        cell2.add("Q" + Principal.vectorCajero[i].getEfectivo());
                        cell3.add(Principal.vectorCajero[i].getUbicacion());
                        if (Principal.vectorCajero[i].isEstado())
                            cell4.add("Activo");
                        else
                            cell4.add("Mantenimiento");
                    }
                }
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                break;

            case 3: //LISTADO DE CLIENTES
                document.add(new Paragraph("LISTADO DE CLIENTES"));
                document.add(new Paragraph(""));

                cell1.add("Nombre");
                cell2.add("Apellido");
                cell3.add("Direccion");
                cell4.add("Telefono");
                for (int i = 0; i < Principal.vectorCliente.length; i++) {
                    if (Principal.vectorCliente[i] != null) {
                        cell1.add(Principal.vectorCliente[i].getNombre());
                        cell2.add(Principal.vectorCliente[i].getApellido());
                        cell3.add(Principal.vectorCliente[i].getDireccion());
                        cell4.add(Integer.toString(Principal.vectorCliente[i].getTelefono()));
                    }
                }
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                break;

            case 4:
            case 5: // TOP 3 CLIENTES CON MAS CUENTAS Y TOP 3 CLIENTES CON MAS DINERO
                if (selection == 5)
                    document.add(new Paragraph("TOP 3 CLIENTES CON MAS DINERO"));
                else
                    document.add(new Paragraph("TOP 3 CLIENTES CON MAS CUENTAS"));
                document.add(new Paragraph(""));
                cell1.add("Nombre");
                if (selection == 5)
                    cell2.add("Dinero");
                else
                    cell2.add("No. Cuentas");

                int[][] ordenados = new int[Principal.vectorCliente.length][2];
                for (int contador = 0; contador < Principal.vectorCliente.length; contador++) {
                    if (Principal.vectorCliente[contador] != null) {
                        ordenados[contador][0] = Principal.vectorCliente[contador].getId();
                        int cuentas = 0;
                        for (int i = 0; i < Principal.vectorCuenta.length; i++) {
                            if (Principal.vectorCuenta[i] != null) {
                                if (Principal.vectorCuenta[i].getCliente().getId() == Principal.vectorCliente[contador].getId()) {
                                    if (selection == 5)
                                        cuentas += Principal.vectorCuenta[i].getMontoFinal();
                                    else
                                        cuentas++;
                                }
                            }
                        }
                        ordenados[contador][1] = cuentas;
                    }

                }

                for (int i = 0; i < ordenados.length - 1; i++) {
                    for (int a = 0; a < ordenados.length - i - 1; a++) {
                        if (ordenados[a][1] < ordenados[a + 1][1]) {
                            int temporal = ordenados[a + 1][1];
                            int idCliente = ordenados[a + 1][0];
                            ordenados[a + 1][1] = ordenados[a][1];
                            ordenados[a + 1][0] = ordenados[a][0];
                            ordenados[a][1] = temporal;
                            ordenados[a][0] = idCliente;
                        }
                    }
                }

                for (int i = 0; i < 3; i++) {
                    for (int o = 0; o < Principal.vectorCliente.length; o++) {
                        if (Principal.vectorCliente[o] != null) {
                            if (Principal.vectorCliente[o].getId() == ordenados[i][0]) {
                                cell1.add(Principal.vectorCliente[o].getNombre());
                                cell2.add(Integer.toString(ordenados[i][1]));
                            }
                        }
                    }
                }

                table.addCell(cell1);
                table.addCell(cell2);

                break;

            case 6:
                document.add(new Paragraph("TOP 3 CLIENTES CON MAS DEUDAS"));
                document.add(new Paragraph(""));
                cell1.add("Cliente");
                cell2.add("Deuda");

                int[][] vectorDeudas = new int[Principal.vectorCliente.length][2];
                for (int i = 0; i < Principal.vectorCliente.length; i++) {
                    if (Principal.vectorCliente[i] != null) {
                        vectorDeudas[i][0] = Principal.vectorCliente[i].getId(); // ID del cliente
                        vectorDeudas[i][1] = 0;
                        for (int o = 0; o < Principal.vectorTarjeta.length; o++) {
                            if (Principal.vectorTarjeta[o] != null){
                                if (Principal.vectorTarjeta[o].getCliente().getId() == vectorDeudas[i][0]){
                                    vectorDeudas[i][1] += Principal.vectorTarjeta[o].getMontoAdeudado();
                                }
                            }
                        }
                        for (int o = 0; o < Principal.vectorPrestamo.length; o++) {
                            if (Principal.vectorPrestamo[o] != null){
                                if (Principal.vectorPrestamo[o].getCliente().getId() == vectorDeudas[i][0]){
                                    vectorDeudas[i][1] += Principal.vectorPrestamo[o].getMontoAdeudado();
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < vectorDeudas.length - 1; i++) {
                    for (int a = 0; a < vectorDeudas.length - i - 1; a++) {
                        if (vectorDeudas[a][1] < vectorDeudas[a + 1][1]) {
                            int temporal = vectorDeudas[a + 1][1];
                            int idCliente = vectorDeudas[a + 1][0];
                            vectorDeudas[a + 1][1] = vectorDeudas[a][1];
                            vectorDeudas[a + 1][0] = vectorDeudas[a][0];
                            vectorDeudas[a][1] = temporal;
                            vectorDeudas[a][0] = idCliente;
                        }
                    }
                }

                for (int i = 0; i < 3; i++) {
                    for (int o = 0; o < Principal.vectorCliente.length; o++) {
                        if (Principal.vectorCliente[o] != null) {
                            if (Principal.vectorCliente[o].getId() == vectorDeudas[i][0]) {
                                cell1.add(Principal.vectorCliente[o].getNombre());
                                cell2.add("Q"+vectorDeudas[i][1]);
                            }
                        }
                    }
                }

                table.addCell(cell1);
                table.addCell(cell2);
                break;

            case 7:
                document.add(new Paragraph("TOP 3 AGENCIAS MAS UTILIZADAS"));
                document.add(new Paragraph(""));
                cell1.add("Nombre");
                cell2.add("Veces utilizada");

                int[][] agenciaOrder = new int[Principal.vectorAgencia.length][2];
                for (int contador = 0; contador < Principal.vectorAgencia.length; contador++) {
                    if (Principal.vectorAgencia[contador] != null) {
                        agenciaOrder[contador][0] = Principal.vectorAgencia[contador].getId();
                        int usos = 0;
                        for (int i = 0; i < Principal.vectorTransaccion.length; i++) {
                            if (Principal.vectorTransaccion[i] != null) {
                                if (Principal.vectorTransaccion[i].getAgencia() != null) {
                                    if (Principal.vectorTransaccion[i].getAgencia().getId() == Principal.vectorAgencia[contador].getId()) {
                                        usos++;
                                    }
                                }

                            }
                        }
                        agenciaOrder[contador][1] = usos;

                    }

                }

                for (int i = 0; i < agenciaOrder.length - 1; i++) {
                    for (int a = 0; a < agenciaOrder.length - i - 1; a++) {
                        if (agenciaOrder[a][1] < agenciaOrder[a + 1][1]) {
                            int temporal = agenciaOrder[a + 1][1];
                            int idAgencia = agenciaOrder[a + 1][0];
                            agenciaOrder[a + 1][1] = agenciaOrder[a][1];
                            agenciaOrder[a + 1][0] = agenciaOrder[a][0];
                            agenciaOrder[a][1] = temporal;
                            agenciaOrder[a][0] = idAgencia;
                        }
                    }
                }

                for (int i = 0; i < 3; i++) {
                    for (int o = 0; o < Principal.vectorAgencia.length; o++) {
                        if (Principal.vectorAgencia[o] != null) {
                            if (Principal.vectorAgencia[o].getId() == agenciaOrder[i][0]) {
                                cell1.add(Principal.vectorAgencia[o].getNombre());
                                cell2.add(Integer.toString(agenciaOrder[i][1]));
                            }

                        }
                    }
                }

                table.addCell(cell1);
                table.addCell(cell2);
                break;

            case 8:
                document.add(new Paragraph("TOP 2 OPERACIONES MAS REALIZADAS EN CALL CENTER"));
                document.add(new Paragraph(""));
                cell1.add("Operacion");
                cell2.add("Veces utilizada");

                int[][] operacionesOrder = new int[3][2];
                operacionesOrder[0][0] = 1;
                operacionesOrder[0][1] = 0;
                operacionesOrder[1][0] = 2;
                operacionesOrder[1][1] = 0;
                operacionesOrder[2][0] = 3;
                operacionesOrder[2][1] = 0;

                for (int i = 0; i < Principal.vectorTransaccion.length; i++) {
                    if (Principal.vectorTransaccion[i] != null) {
                        if (Principal.vectorTransaccion[i].getCallCenter() != null) {
                            if (Principal.vectorTransaccion[i].getTipoOperacion().equals("PagoServicio"))
                                operacionesOrder[0][1]++;
                            else if (Principal.vectorTransaccion[i].getTipoOperacion().equals("PagoPrestamo"))
                                operacionesOrder[1][1]++;
                            else if (Principal.vectorTransaccion[i].getTipoOperacion().equals("PagoTarjeta"))
                                operacionesOrder[2][1]++;
                        }
                    }
                }

                for (int i = 0; i < operacionesOrder.length - 1; i++) {
                    for (int a = 0; a < operacionesOrder.length - i - 1; a++) {
                        if (operacionesOrder[a][1] < operacionesOrder[a + 1][1]) {
                            int temporal = operacionesOrder[a + 1][1];
                            int idOperacion = operacionesOrder[a + 1][0];
                            operacionesOrder[a + 1][1] = operacionesOrder[a][1];
                            operacionesOrder[a + 1][0] = operacionesOrder[a][0];
                            operacionesOrder[a][1] = temporal;
                            operacionesOrder[a][0] = idOperacion;
                        }
                    }
                }

                for (int i = 0; i < 2; i++) {
                    if (operacionesOrder[i][0] == 1)
                        cell1.add("Servicios");
                    else if (operacionesOrder[i][0] == 2)
                        cell1.add("Prestamo");
                    else if (operacionesOrder[i][0] == 3)
                        cell1.add("Tarjeta");
                    cell2.add("" + operacionesOrder[i][1]);
                }

                table.addCell(cell1);
                table.addCell(cell2);
                break;

            case 9:
            case 10: // SUMA DE EFECTIVO Y EFECTIVO DE AGENCIAS
                if (selection == 10)
                    document.add(new Paragraph("FECTIVO DE AGENCIAS"));
                else
                    document.add(new Paragraph("SUMA DE FECTIVO DE AGENCIAS"));
                document.add(new Paragraph(""));

                cell1.add("Agencia");
                cell2.add("Efectivo");
                int efectivo = 0;
                for (int i = 0; i < Principal.vectorAgencia.length; i++) {
                    if (Principal.vectorAgencia[i] != null) {
                        cell1.add(Principal.vectorAgencia[i].getNombre());
                        cell2.add("Q" + Principal.vectorAgencia[i].getEfectivo());
                        efectivo += Principal.vectorAgencia[i].getEfectivo();
                    }
                }
                table.addCell(cell1);
                table.addCell(cell2);
                if (selection == 9) {
                    document.add(new Paragraph(""));
                    document.add(new Paragraph("Efectivo total en agencias: Q" + efectivo));
                }
                break;

            case 11:
            case 12: // LISTADO DE EMPLEADOS DE AGENCIAS
                document.add(new Paragraph("LISTADO DE EMPLEADOS POR AGENCIA"));
                document.add(new Paragraph(""));

                cell1.add("Nombre");
                cell2.add("Apellido");
                cell3.add("Telefono");
                cell4.add("Trabaja en");

                for (int i = 0; i < Principal.vectorEmpleado.length; i++) {
                    if (Principal.vectorEmpleado[i] != null) {
                        cell1.add(Principal.vectorEmpleado[i].getNombre());
                        cell2.add(Principal.vectorEmpleado[i].getApellido());
                        cell3.add(Integer.toString(Principal.vectorEmpleado[i].getTelefono()));
                        for (int o = 0; o < Principal.vectorAgencia.length; o++) {
                            if (Principal.vectorAgencia[o] != null) {
                                for (int employ = 0; employ < Principal.vectorAgencia[o].getEmpleado().length; employ++) {
                                    if (Principal.vectorAgencia[o].getEmpleado()[employ] != null) {
                                        if (Principal.vectorAgencia[o].getEmpleado()[employ].getId() == Principal.vectorEmpleado[i].getId())
                                            cell4.add(Principal.vectorAgencia[o].getNombre());
                                    }

                                }

                            }
                        }
                        for (int o = 0; o < Principal.vectorAutobanco.length; o++) {
                            if (Principal.vectorAutobanco[o] != null) {
                                for (int employ = 0; employ < Principal.vectorAutobanco[o].getEmpleado().length; employ++) {
                                    if (Principal.vectorAutobanco[o].getEmpleado()[employ] != null) {
                                        if (Principal.vectorAutobanco[o].getEmpleado()[employ].getId() == Principal.vectorEmpleado[i].getId())
                                            cell4.add(Principal.vectorAutobanco[o].getNombre());
                                    }

                                }

                            }
                        }
                        for (int o = 0; o < Principal.callCenter.getEmpleado().length; o++) {
                            if (Principal.callCenter.getEmpleado()[o] != null) {
                                if (Principal.callCenter.getEmpleado()[o].getId() == Principal.vectorEmpleado[i].getId())
                                    cell4.add(Principal.callCenter.getNombre());
                            }
                        }
                    }
                }

                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                break;

            case 13: // AGENCIA CON MAS EMPLEADOS
                document.add(new Paragraph("AGENCIA CON MAS EMPLEADOS"));
                document.add(new Paragraph(""));

                int[][] datosOrdenados = new int[Principal.vectorAgencia.length][2];
                for (int contador = 0; contador < Principal.vectorAgencia.length; contador++) {
                    if (Principal.vectorAgencia[contador] != null) {
                        datosOrdenados[contador][0] = Principal.vectorAgencia[contador].getId();
                        int empleados = 0;
                        for (int i = 0; i < Principal.vectorAgencia[contador].getEmpleado().length; i++) {
                            if (Principal.vectorAgencia[contador].getEmpleado()[i] != null) {
                                empleados++;
                            }
                        }
                        datosOrdenados[contador][1] = empleados;
                    }

                }

                for (int i = 0; i < datosOrdenados.length - 1; i++) {
                    for (int a = 0; a < datosOrdenados.length - i - 1; a++) {
                        if (datosOrdenados[a][1] < datosOrdenados[a + 1][1]) {
                            int temporal = datosOrdenados[a + 1][1];
                            int idAgencia = datosOrdenados[a + 1][0];
                            datosOrdenados[a + 1][1] = datosOrdenados[a][1];
                            datosOrdenados[a + 1][0] = datosOrdenados[a][0];
                            datosOrdenados[a][1] = temporal;
                            datosOrdenados[a][0] = idAgencia;
                        }
                    }
                }

                for (int i = 0; i < Principal.vectorAgencia.length; i++) {
                    if (Principal.vectorAgencia[i] != null) {
                        if (Principal.vectorAgencia[i].getId() == datosOrdenados[0][0])
                            document.add(new Paragraph("La agencia con mas empleados es la " + Principal.vectorAgencia[i].getNombre() +
                                    " con " + datosOrdenados[0][1] + " empleados."));
                    }
                }

                break;

            case 14: // CLIENTES CON MAS COMPRAS EN TARJETA
                document.add(new Paragraph("CLIENTES CON MAS COMPRAS EN TARJETA"));
                document.add(new Paragraph(""));
                cell1.add("Cliente");
                cell2.add("No. Tarjeta");
                cell3.add("Veces utilizada");

                int[][] tarjetaOrder = new int[Principal.vectorTarjeta.length][2];
                for (int contador = 0; contador < Principal.vectorTarjeta.length; contador++) {
                    if (Principal.vectorTarjeta[contador] != null) {
                        tarjetaOrder[contador][0] = Principal.vectorTarjeta[contador].getNoTarjeta();
                        tarjetaOrder[contador][1] = Principal.vectorTarjeta[contador].getNoCompras();
                    }

                }

                for (int i = 0; i < tarjetaOrder.length - 1; i++) {
                    for (int a = 0; a < tarjetaOrder.length - i - 1; a++) {
                        if (tarjetaOrder[a][1] < tarjetaOrder[a + 1][1]) {
                            int temporal = tarjetaOrder[a + 1][1];
                            int idTarjeta = tarjetaOrder[a + 1][0];
                            tarjetaOrder[a + 1][1] = tarjetaOrder[a][1];
                            tarjetaOrder[a + 1][0] = tarjetaOrder[a][0];
                            tarjetaOrder[a][1] = temporal;
                            tarjetaOrder[a][0] = idTarjeta;
                        }
                    }
                }

                for (int i = 0; i < 3; i++) {
                    for (int o = 0; o < Principal.vectorTarjeta.length; o++) {
                        if (Principal.vectorTarjeta[o] != null) {
                            if (Principal.vectorTarjeta[o].getNoTarjeta() == tarjetaOrder[i][0]) {
                                cell1.add(Principal.vectorTarjeta[o].getCliente().getNombre());
                                cell2.add("" + Principal.vectorTarjeta[o].getNoTarjeta());
                                cell3.add(Integer.toString(tarjetaOrder[i][1]));
                            }

                        }
                    }
                }
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                break;
        }

        document.add(table);

        // Close document
        document.close();

        File myFile = new File(destination);
        Desktop.getDesktop().open(myFile);
    }

}
