package usac.ipc1.beans;

public class Empleado extends Usuario {

    private int id;
    private int telefono;

    public Empleado(){

    }

    public Empleado(int id, int telefono) {
        this.id = id;
        this.telefono = telefono;
    }

    public Empleado(String nombre, String apellido, int id, int telefono) {
        super(nombre, apellido);
        this.id = id;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}
