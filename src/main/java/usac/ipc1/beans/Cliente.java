package usac.ipc1.beans;

public class Cliente extends Usuario {

    protected int id;
    protected String direccion;
    protected int telefono;

    public Cliente(){

    }

    public Cliente(int id, String direccion, int telefono) {
        this.id = id;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public Cliente(String nombre, String apellido, int id, String direccion, int telefono) {
        super(nombre, apellido);
        this.id = id;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}
