package usac.ipc1.beans;

import java.util.Date;

public class Prestamo {

    private int noPrestamo;
    private Cliente cliente;
    private Date fechaPrestamo;
    private double montoPrestado;
    private double montoAdeudado;
    private boolean estado;
    private boolean espera;

    public Prestamo() {
    }

    public Prestamo(int noPrestamo, Cliente cliente, Date fechaPrestamo, double montoPrestado, double montoAdeudado, boolean estado, boolean espera) {
        this.noPrestamo = noPrestamo;
        this.cliente = cliente;
        this.fechaPrestamo = fechaPrestamo;
        this.montoPrestado = montoPrestado;
        this.montoAdeudado = montoAdeudado;
        this.estado = estado;
        this.espera = espera;
    }

    public int getNoPrestamo() {
        return noPrestamo;
    }

    public void setNoPrestamo(int noPrestamo) {
        this.noPrestamo = noPrestamo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public double getMontoPrestado() {
        return montoPrestado;
    }

    public void setMontoPrestado(double montoPrestado) {
        this.montoPrestado = montoPrestado;
    }

    public double getMontoAdeudado() {
        return montoAdeudado;
    }

    public void setMontoAdeudado(double montoAbonado) {
        this.montoAdeudado = montoAbonado;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isEspera() {
        return espera;
    }

    public void setEspera(boolean espera) {
        this.espera = espera;
    }
}
