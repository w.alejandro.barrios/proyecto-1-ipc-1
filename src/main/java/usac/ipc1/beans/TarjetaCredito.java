package usac.ipc1.beans;

import java.util.Date;

public class TarjetaCredito {

    private int noTarjeta;
    private Cliente cliente;
    private Date fechaVencimiento;
    private int limiteCredito;
    private double montoAdeudado;
    private boolean estado; // 1 = aprobada; 0 = Denegada;
    private boolean espera;
    private int noCompras;

    public TarjetaCredito() {
    }

    public TarjetaCredito(int noTarjeta, Cliente cliente, Date fechaVencimiento, int limiteCredito, double montoAdeudado, boolean estado, boolean espera, int noCompras) {
        this.noTarjeta = noTarjeta;
        this.cliente = cliente;
        this.fechaVencimiento = fechaVencimiento;
        this.limiteCredito = limiteCredito;
        this.montoAdeudado = montoAdeudado;
        this.estado = estado;
        this.espera = espera;
        this.noCompras = noCompras;
    }

    public int getNoTarjeta() {
        return noTarjeta;
    }

    public void setNoTarjeta(int noTarjeta) {
        this.noTarjeta = noTarjeta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public int getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(int limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    public double getMontoAdeudado() {
        return montoAdeudado;
    }

    public void setMontoAdeudado(double montoAdeudado) {
        this.montoAdeudado = montoAdeudado;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isEspera() {
        return espera;
    }

    public void setEspera(boolean espera) {
        this.espera = espera;
    }

    public int getNoCompras() {
        return noCompras;
    }

    public void setNoCompras(int noCompras) {
        this.noCompras = noCompras;
    }
}
