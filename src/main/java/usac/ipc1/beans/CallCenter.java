package usac.ipc1.beans;

public class CallCenter {

    private int id;
    private String nombre;
    private Empleado empleados[];

    public CallCenter() {
    }

    public CallCenter(int id, String nombre, Empleado empleados[]) {
        this.id = id;
        this.nombre = nombre;
        this.empleados = empleados;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Empleado[] getEmpleado(){
        return empleados;
    }

    public void setEmpleados(Empleado[] empleados){
        this.empleados = empleados;
    }
}
