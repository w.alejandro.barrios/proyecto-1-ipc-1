package usac.ipc1.beans;

import java.util.Date;

public class Cuenta {

    private int noCuenta;
    private Cliente cliente;
    private Date fechaApertura;
    private double montoInicial;
    private double montoFinal;
    private boolean tipoCuenta; // 0 = ahorro; 1 = monetaria;

    public Cuenta() {
    }

    public Cuenta(int noCuenta, Cliente cliente, Date fechaApertura, double montoInicial, double montoFinal, boolean tipoCuenta) {
        this.noCuenta = noCuenta;
        this.cliente = cliente;
        this.fechaApertura = fechaApertura;
        this.montoInicial = montoInicial;
        this.montoFinal = montoFinal;
        this.tipoCuenta = tipoCuenta;
    }

    public int getNoCuenta() {
        return noCuenta;
    }

    public void setNoCuenta(int noCuenta) {
        this.noCuenta = noCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public double getMontoInicial() {
        return montoInicial;
    }

    public void setMontoInicial(double montoInicial) {
        this.montoInicial = montoInicial;
    }

    public double getMontoFinal() {
        return montoFinal;
    }

    public void setMontoFinal(double montoFinal) {
        this.montoFinal = montoFinal;
    }

    public boolean isTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(boolean tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
}
