package usac.ipc1.beans;

public class Autobanco extends Agencia {

    protected int noCajasAutobanco;

    public Autobanco(){

    }

    public Autobanco(int noCajasAutobanco) {
        this.noCajasAutobanco = noCajasAutobanco;
    }

    public Autobanco(String nombre, int id, String direccion, int telefono, int noCajas, int noEscritorios, double efectivo, Empleado[] empleados, int noCajasAutobanco) {
        super(nombre, id, direccion, telefono, noCajas, noEscritorios, efectivo, empleados);
        this.noCajasAutobanco = noCajasAutobanco;
    }

    public int getNoCajasAutobanco() {
        return noCajasAutobanco;
    }

    public void setNoCajasAutobanco(int noCajasAutobanco) {
        this.noCajasAutobanco = noCajasAutobanco;
    }
}
