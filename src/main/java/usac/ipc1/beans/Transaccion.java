package usac.ipc1.beans;

import usac.ipc1.beans.Cliente;

import java.util.Date;

public class Transaccion {

    private int id;
    private Cliente cliente;
    private Date fechaTransaccion;
    private double monto;
    private boolean tipo; // 0 = efectivo, 1 = cheque
    private String tipoOperacion; // Deposito, Retiro, CambioCheque, PagoServicio, PagoTarjeta, PagoPrestamo, RetiroCajero
    // ----- Deposito
    private int cuentaDestino;
    // ----- Retiro
    private int cuentaRetiro;
    // ----- PagoServicio
    private String servicio; // Agua, Luz, Telefono
    // ----- PagoTarjeta
    private int noTarjeta;
    // ----- PagoPrestamo
    private int noPrestamo;
    // ----- Lugar
    private Agencia agencia;
    private Autobanco autobanco;
    private Cajero cajero;
    private CallCenter callCenter;

    public Transaccion() {
    }

    // Deposito
    public Transaccion(int id, Cliente cliente, Date fechaTransaccion, double monto, boolean tipo, String tipoOperacion, int cuentaDestino) {
        this.id = id;
        this.cliente = cliente;
        this.fechaTransaccion = fechaTransaccion;
        this.monto = monto;
        this.tipo = tipo;
        this.tipoOperacion = tipoOperacion;
        this.cuentaDestino = cuentaDestino;
    }


    // Retiro y RetiroCajero
    public Transaccion(int id, Cliente cliente, Date fechaTransaccion, double monto, String tipoOperacion, int cuentaRetiro) {
        this.id = id;
        this.cliente = cliente;
        this.fechaTransaccion = fechaTransaccion;
        this.monto = monto;
        this.tipoOperacion = tipoOperacion;
        this.cuentaRetiro = cuentaRetiro;
    }

    // CambioCheque
    public Transaccion(int id, Cliente cliente, Date fechaTransaccion, double monto, String tipoOperacion, int cuentaDestino, int cuentaRetiro) {
        this.id = id;
        this.cliente = cliente;
        this.fechaTransaccion = fechaTransaccion;
        this.monto = monto;
        this.tipoOperacion = tipoOperacion;
        this.cuentaDestino = cuentaDestino;
        this.cuentaRetiro = cuentaRetiro;
    }

    // PagoServicio
    public Transaccion(int id, Cliente cliente, Date fechaTransaccion, double monto, boolean tipo, String tipoOperacion, String servicio) {
        this.id = id;
        this.cliente = cliente;
        this.fechaTransaccion = fechaTransaccion;
        this.monto = monto;
        this.tipo = tipo;
        this.tipoOperacion = tipoOperacion;
        this.servicio = servicio;
    }

    // PagoTarjeta
    public Transaccion(int id, Cliente cliente, Date fechaTransaccion, double monto, boolean tipo, int noTarjeta, String tipoOperacion){
        this.id = id;
        this.cliente = cliente;
        this.fechaTransaccion = fechaTransaccion;
        this.monto = monto;
        this.tipo = tipo;
        this.tipoOperacion = tipoOperacion;
        this.noTarjeta = noTarjeta;
    }

    //PagoPrestamo
    public Transaccion(int id, Cliente cliente, Date fechaTransaccion, double monto, int noPrestamo, boolean tipo, String tipoOperacion){
        this.id = id;
        this.cliente = cliente;
        this.fechaTransaccion = fechaTransaccion;
        this.monto = monto;
        this.tipo = tipo;
        this.tipoOperacion = tipoOperacion;
        this.noPrestamo = noPrestamo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(Date fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public int getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(int cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public int getCuentaRetiro() {
        return cuentaRetiro;
    }

    public void setCuentaRetiro(int cuentaRetiro) {
        this.cuentaRetiro = cuentaRetiro;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public int getNoTarjeta() {
        return noTarjeta;
    }

    public void setNoTarjeta(int noTarjeta) {
        this.noTarjeta = noTarjeta;
    }

    public int getNoPrestamo() {
        return noPrestamo;
    }

    public void setNoPrestamo(int noPrestamo) {
        this.noPrestamo = noPrestamo;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public Autobanco getAutobanco() {
        return autobanco;
    }

    public void setAutobanco(Autobanco autobanco) {
        this.autobanco = autobanco;
    }

    public Cajero getCajero() {
        return cajero;
    }

    public void setCajero(Cajero cajero) {
        this.cajero = cajero;
    }

    public CallCenter getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(CallCenter callCenter) {
        this.callCenter = callCenter;
    }
}
