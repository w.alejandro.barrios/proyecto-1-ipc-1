package usac.ipc1.beans;

public class Agencia {

    protected String nombre;
    protected int id;
    protected String direccion;
    protected int telefono;
    protected int noCajas;
    protected int noEscritorios;
    protected double efectivo;
    protected Empleado empleados[];

    public Agencia() {
    }

    public Agencia(String nombre, int id, String direccion, int telefono, int noCajas, int noEscritorios, double efectivo, Empleado empleados[]) {
        this.nombre = nombre;
        this.id = id;
        this.direccion = direccion;
        this.telefono = telefono;
        this.noCajas = noCajas;
        this.noEscritorios = noEscritorios;
        this.efectivo = efectivo;
        this.empleados = empleados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getNoCajas() {
        return noCajas;
    }

    public void setNoCajas(int noCajas) {
        this.noCajas = noCajas;
    }

    public int getNoEscritorios() {
        return noEscritorios;
    }

    public void setNoEscritorios(int noEscritorios) {
        this.noEscritorios = noEscritorios;
    }

    public double getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(double efectivo) {
        this.efectivo = efectivo;
    }

    public Empleado[] getEmpleado(){
        return empleados;
    }

    public void setEmpleados(Empleado[] empleados){
        this.empleados = empleados;
    }
}
