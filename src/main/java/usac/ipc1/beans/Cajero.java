package usac.ipc1.beans;

public class Cajero {

    private int id;
    private String ubicacion;
    private double efectivo;
    private boolean estado; // 0 = mantenimiento; 1 = activo;
    private int noTransaccionesProcesadas;

    public Cajero() {
    }

    public Cajero(int id, String ubicacion, double efectivo, boolean estado, int noTransaccionesProcesadas) {
        this.id = id;
        this.ubicacion = ubicacion;
        this.efectivo = efectivo;
        this.estado = estado;
        this.noTransaccionesProcesadas = noTransaccionesProcesadas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public double getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(double efectivo) {
        this.efectivo = efectivo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getNoTransaccionesProcesadas() {
        return noTransaccionesProcesadas;
    }

    public void setNoTransaccionesProcesadas(int noTransaccionesProcesadas) {
        this.noTransaccionesProcesadas = noTransaccionesProcesadas;
    }
}
