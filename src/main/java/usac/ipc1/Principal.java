package usac.ipc1;

import usac.ipc1.beans.*;
import usac.ipc1.beans.Transaccion;

public class Principal {

    public static Cliente[] vectorCliente;
    public static Agencia[] vectorAgencia;
    public static Autobanco[] vectorAutobanco;
    public static Cajero[] vectorCajero;
    public static Empleado[] vectorEmpleado;
    public static Cuenta[] vectorCuenta;
    public static Transaccion[] vectorTransaccion;
    public static TarjetaCredito[] vectorTarjeta;
    public static Prestamo[] vectorPrestamo;
    public static CallCenter callCenter;
    public static int contadorClientes;
    public static int contadorAgencia;
    public static int contadorAutobanco;
    public static int contadorCajero;
    public static int contadorEmpleado;
    public static int contadorCuenta;
    public static int contadorTransaccion;
    public static int contadorTarjeta;
    public static int contadorPrestamo;

    // ClientSelected
    public static Cliente clienteChoose;

}
