package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Autobanco;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class AutobancoDialog extends JDialog {

    private Autobanco[] autobanco;
    private boolean isEdited = false;
    private int position;

    public AutobancoDialog(JFrame padre, boolean modal, Autobanco autobanco[], Table table, int position){
        super(padre, modal);
        setBounds(new Rectangle(500,210));
        setLocationRelativeTo(null);
        setTitle("Editar Agencia");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.autobanco = autobanco;
        this.position = position;
        isEdited = true;

        buildDialog(table);
    }

    public AutobancoDialog(JFrame padre, boolean modal, Autobanco autobanco[], Table table){
        super(padre, modal);
        setBounds(new Rectangle(500,210));
        setLocationRelativeTo(null);
        setTitle("Editar Agencia");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.autobanco = autobanco;

        buildDialog(table);
    }

    private void buildDialog(Table table){
        JLabel lblName = new JLabel("Nombre");
        JTextField txtName = new JTextField();

        JLabel lblDirection = new JLabel("Direccion");
        JTextField txtDirection = new JTextField();

        JLabel lblPhone = new JLabel("Telefono");
        JTextField txtPhone = new JTextField();

        JLabel lblNoReceptors = new JLabel("No. Cajas");
        JTextField txtNoReceptors = new JTextField();

        JLabel lblDesks = new JLabel("No. Escritorios");
        JTextField txtDesks = new JTextField();

        JLabel lblMoney = new JLabel("Efectivo");
        JTextField txtMoney = new JTextField();

        JLabel lblAutoReceptors = new JLabel("Auto Cajas");
        JTextField txtAutoReceptors = new JTextField();

        if (isEdited){
            txtName.setText(autobanco[position].getNombre());
            txtDirection.setText(autobanco[position].getDireccion());
            txtPhone.setText(Integer.toString(autobanco[position].getTelefono()));
            txtNoReceptors.setText(Integer.toString(autobanco[position].getNoCajas()));
            txtDesks.setText(Integer.toString(autobanco[position].getNoEscritorios()));
            txtMoney.setText(Double.toString(autobanco[position].getEfectivo()));
            txtAutoReceptors.setText(Integer.toString(autobanco[position].getNoCajasAutobanco()));
        }

        JButton btnSave = new JButton("Guardar");
        btnSave.addActionListener(e -> {
            if (isEdited){
                autobanco[position].setNombre(txtName.getText());
                autobanco[position].setDireccion(txtDirection.getText());
                autobanco[position].setTelefono(Integer.parseInt(txtPhone.getText()));
                autobanco[position].setNoCajas(Integer.parseInt(txtNoReceptors.getText()));
                autobanco[position].setNoEscritorios(Integer.parseInt(txtDesks.getText()));
                autobanco[position].setEfectivo(Double.parseDouble(txtMoney.getText()));
                autobanco[position].setNoCajasAutobanco(Integer.parseInt(txtAutoReceptors.getText()));
                table.editar(autobanco[position], position);
                JOptionPane.showMessageDialog(AutobancoDialog.this, "Cambios efectuados exitosamente");
            }else{
                Autobanco nueva = new Autobanco();
                nueva.setId(Principal.contadorAutobanco);
                nueva.setNombre(txtName.getText());
                nueva.setDireccion(txtDirection.getText());
                nueva.setTelefono(Integer.parseInt(txtPhone.getText()));
                nueva.setNoCajas(Integer.parseInt(txtNoReceptors.getText()));
                nueva.setNoEscritorios(Integer.parseInt(txtDesks.getText()));
                nueva.setEfectivo(Double.parseDouble(txtMoney.getText()));
                nueva.setNoCajasAutobanco(Integer.parseInt(txtAutoReceptors.getText()));
                table.agregar(nueva);
                JOptionPane.showMessageDialog(AutobancoDialog.this, "Se almaceno la agencia");
            }
            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblName);
        contenedor1.add(txtName);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblDirection);
        contenedor2.add(txtDirection);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblPhone);
        contenedor3.add(txtPhone);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblNoReceptors);
        contenedor4.add(txtNoReceptors);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500,50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(lblDesks);
        contenedor5.add(txtDesks);

        // Contenedor 6
        JPanel contenedor6 = new JPanel();
        contenedor6.setBackground(Color.WHITE);
        contenedor6.setSize(500,50);
        BoxLayout boxLayout6 = new BoxLayout(contenedor6, BoxLayout.X_AXIS);
        contenedor6.setLayout(boxLayout6);
        contenedor6.add(lblMoney);
        contenedor6.add(txtMoney);

        // Contenedor 7
        JPanel contenedor7 = new JPanel();
        contenedor7.setBackground(Color.WHITE);
        contenedor7.setSize(500,50);
        BoxLayout boxLayout7 = new BoxLayout(contenedor7, BoxLayout.X_AXIS);
        contenedor7.setLayout(boxLayout7);
        contenedor7.add(lblAutoReceptors);
        contenedor7.add(txtAutoReceptors);

        // Contenedor 8
        JPanel contenedor8 = new JPanel();
        contenedor8.setBackground(Color.WHITE);
        contenedor8.setSize(500,50);
        BoxLayout boxLayout8 = new BoxLayout(contenedor8, BoxLayout.X_AXIS);
        contenedor8.setLayout(boxLayout8);
        contenedor8.add(btnSave);
        contenedor8.add(btnCancel);

        JPanel contenedor9 = new JPanel();
        contenedor9.setBackground(Color.white);
        contenedor9.setSize(500, 50);
        BoxLayout boxlayout9 = new BoxLayout(contenedor9, BoxLayout.Y_AXIS);
        contenedor9.setLayout(boxlayout9);
        contenedor9.add(contenedor1);
        contenedor9.add(contenedor2);
        contenedor9.add(contenedor3);
        contenedor9.add(contenedor4);
        contenedor9.add(contenedor5);
        contenedor9.add(contenedor6);
        contenedor9.add(contenedor7);
        contenedor9.add(contenedor8);

        add(contenedor9);
    }

}
