package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Cliente;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class ClienteDialog extends JDialog {

    private Cliente[] vectorCliente;
    private boolean isEdited = false;
    private int position;

    public ClienteDialog(JFrame padre, boolean modal, Cliente vectorCliente[], Table table, int position){
        super(padre, modal);
        setBounds(new Rectangle(500,150));
        setLocationRelativeTo(null);
        setTitle("Editar Cliente");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorCliente = vectorCliente;
        this.position = position;
        isEdited = true;

        buildDialog(table);
    }

    public ClienteDialog(JFrame padre, boolean modal, Cliente vectorCliente[], Table table){
        super(padre, modal);
        setBounds(new Rectangle(500,150));
        setLocationRelativeTo(null);
        setTitle("Cliente Nuevo");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorCliente = vectorCliente;

        buildDialog(table);

    }

    private void buildDialog(Table table){
        JLabel lblName = new JLabel("Nombre");
        JTextField txtName = new JTextField();

        JLabel lblLastName = new JLabel("Apellido");
        JTextField txtLastName = new JTextField();

        JLabel lblDirection = new JLabel("Direccion");
        JTextField txtDirection = new JTextField();

        JLabel lblPhone = new JLabel("Telefono");
        JTextField txtPhone = new JTextField();
        if (isEdited){
            txtName.setText(vectorCliente[position].getNombre());
            txtLastName.setText(vectorCliente[position].getApellido());
            txtDirection.setText(vectorCliente[position].getDireccion());
            txtPhone.setText(Integer.toString(vectorCliente[position].getTelefono()));
        }

        JButton btnSave = new JButton("Guardar");
        btnSave.addActionListener(e -> {
            if(isEdited){
                vectorCliente[position].setNombre(txtName.getText());
                vectorCliente[position].setApellido(txtLastName.getText());
                vectorCliente[position].setDireccion(txtDirection.getText());
                vectorCliente[position].setTelefono(Integer.parseInt(txtPhone.getText()));
                table.editar(vectorCliente[position], position);
                JOptionPane.showMessageDialog(ClienteDialog.this, "Cambios efectuados exitosamente");
            }else{
                Cliente nuevo = new Cliente();
                nuevo.setId(Principal.contadorClientes);
                nuevo.setNombre(txtName.getText());
                nuevo.setApellido(txtLastName.getText());
                nuevo.setDireccion(txtDirection.getText());
                nuevo.setTelefono(Integer.parseInt(txtPhone.getText()));
                table.agregar(nuevo);
                JOptionPane.showMessageDialog(ClienteDialog.this, "Se almaceno el cliente");
            }

            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblName);
        contenedor1.add(txtName);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblLastName);
        contenedor2.add(txtLastName);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblDirection);
        contenedor3.add(txtDirection);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblPhone);
        contenedor4.add(txtPhone);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500,50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(btnSave);
        contenedor5.add(btnCancel);

        JPanel contenedor6 = new JPanel();
        contenedor6.setBackground(Color.white);
        contenedor6.setSize(500, 50);
        BoxLayout boxlayout6 = new BoxLayout(contenedor6, BoxLayout.Y_AXIS);
        contenedor6.setLayout(boxlayout6);
        contenedor6.add(contenedor1);
        contenedor6.add(contenedor2);
        contenedor6.add(contenedor3);
        contenedor6.add(contenedor4);
        contenedor6.add(contenedor5);

        add(contenedor6);
    }

}
