package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Cajero;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class CajeroDialog extends JDialog {

    private Cajero[] vectorCajero;
    private boolean isEdited = false;
    private int position;

    public CajeroDialog(JFrame padre, boolean modal, Cajero vectorCajero[], Table table, int position){
        super(padre, modal);
        setBounds(new Rectangle(500,150));
        setLocationRelativeTo(null);
        setTitle("Editar Agencia");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorCajero = vectorCajero;
        this.position = position;
        isEdited = true;

        buildDialog(table);
    }

    public CajeroDialog(JFrame padre, boolean modal, Cajero vectorCajero[], Table table){
        super(padre, modal);
        setBounds(new Rectangle(500,150));
        setLocationRelativeTo(null);
        setTitle("Editar Agencia");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorCajero = vectorCajero;

        buildDialog(table);
    }

    private void buildDialog(Table table){
        JLabel lblLocation = new JLabel("Ubicacion");
        JTextField txtLocation = new JTextField();

        JLabel lblMoney = new JLabel("Efectivo");
        JTextField txtMoney = new JTextField();

        JLabel lblState = new JLabel("State");
        JComboBox cbState = new JComboBox();
        cbState.addItem("Activo");
        cbState.addItem("Mantenimiento");

        if(isEdited){
            txtLocation.setText(vectorCajero[position].getUbicacion());
            txtMoney.setText(Double.toString(vectorCajero[position].getEfectivo()));
            if(vectorCajero[position].isEstado()){
                cbState.setSelectedItem("Activo");
            }else{
                cbState.setSelectedItem("Mantenimiento");
            }
        }

        JButton btnSave = new JButton("Guardar");
        btnSave.addActionListener(e -> {
            if (isEdited){
                vectorCajero[position].setUbicacion(txtLocation.getText());
                vectorCajero[position].setEfectivo(Double.parseDouble(txtMoney.getText()));
                if (cbState.getSelectedItem() == "Activo"){
                    vectorCajero[position].setEstado(true);
                }else{
                    vectorCajero[position].setEstado(false);
                }
                table.editar(vectorCajero[position], position);
                JOptionPane.showMessageDialog(CajeroDialog.this, "Cambios efectuados exitosamente");
            }else{
                Cajero cajero = new Cajero();
                cajero.setId(Principal.contadorCajero);
                cajero.setUbicacion(txtLocation.getText());
                cajero.setEfectivo(Double.parseDouble(txtMoney.getText()));
                if (cbState.getSelectedItem() == "Activo"){
                    cajero.setEstado(true);
                }else{
                    cajero.setEstado(false);
                }
                cajero.setNoTransaccionesProcesadas(0);
                table.agregar(cajero);
                JOptionPane.showMessageDialog(CajeroDialog.this, "Se almaceno el cajero");
            }
            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblLocation);
        contenedor1.add(txtLocation);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblMoney);
        contenedor2.add(txtMoney);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblState);
        contenedor3.add(cbState);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(btnSave);
        contenedor4.add(btnCancel);

        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.white);
        contenedor5.setSize(500, 50);
        BoxLayout boxlayout5 = new BoxLayout(contenedor5, BoxLayout.Y_AXIS);
        contenedor5.setLayout(boxlayout5);
        contenedor5.add(contenedor1);
        contenedor5.add(contenedor2);
        contenedor5.add(contenedor3);
        contenedor5.add(contenedor4);

        add(contenedor5);
    }

}
