package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Empleado;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class EmpleadoDialog extends JDialog {

    private Empleado[] vectorEmpleado;
    private boolean isEdited = false;
    private int position;

    public EmpleadoDialog(JFrame padre, boolean modal, Empleado vectorEmpleado[], Table table, int position){
        super(padre, modal);
        setBounds(new Rectangle(500,150));
        setLocationRelativeTo(null);
        setTitle("Editar Cliente");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorEmpleado = vectorEmpleado;
        this.position = position;
        isEdited = true;

        buildDialog(table);
    }

    public EmpleadoDialog(JFrame padre, boolean modal, Empleado vectorEmpleado[], Table table){
        super(padre, modal);
        setBounds(new Rectangle(500,150));
        setLocationRelativeTo(null);
        setTitle("Cliente Nuevo");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorEmpleado = vectorEmpleado;

        buildDialog(table);

    }

    private void buildDialog(Table table) {
        JLabel lblName = new JLabel("Nombre");
        JTextField txtName = new JTextField();

        JLabel lblLastName = new JLabel("Apellido");
        JTextField txtLastName = new JTextField();

        JLabel lblPhone = new JLabel("Telefono");
        JTextField txtPhone = new JTextField();

        if (isEdited) {
            txtName.setText(vectorEmpleado[position].getNombre());
            txtLastName.setText(vectorEmpleado[position].getApellido());
            txtPhone.setText(Integer.toString(vectorEmpleado[position].getTelefono()));
        }

        JButton btnSave = new JButton("Guardar");
        btnSave.addActionListener(e -> {
            if(isEdited){
                vectorEmpleado[position].setNombre(txtName.getText());
                vectorEmpleado[position].setApellido(txtLastName.getText());
                vectorEmpleado[position].setTelefono(Integer.parseInt(txtPhone.getText()));
                table.editar(vectorEmpleado[position], position);
                JOptionPane.showMessageDialog(EmpleadoDialog.this, "Cambios efectuados exitosamente");
            }else{
                Empleado nuevo = new Empleado();
                nuevo.setId(Principal.contadorEmpleado);
                nuevo.setNombre(txtName.getText());
                nuevo.setApellido(txtLastName.getText());
                nuevo.setTelefono(Integer.parseInt(txtPhone.getText()));
                table.agregar(nuevo);
                JOptionPane.showMessageDialog(EmpleadoDialog.this, "Se almaceno el empleado");
            }

            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblName);
        contenedor1.add(txtName);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblLastName);
        contenedor2.add(txtLastName);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblPhone);
        contenedor3.add(txtPhone);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(btnSave);
        contenedor4.add(btnCancel);

        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.white);
        contenedor5.setSize(500, 50);
        BoxLayout boxlayout5 = new BoxLayout(contenedor5, BoxLayout.Y_AXIS);
        contenedor5.setLayout(boxlayout5);
        contenedor5.add(contenedor1);
        contenedor5.add(contenedor2);
        contenedor5.add(contenedor3);
        contenedor5.add(contenedor4);

        add(contenedor5);

    }
}
