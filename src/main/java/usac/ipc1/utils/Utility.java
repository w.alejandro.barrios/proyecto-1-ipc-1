package usac.ipc1.utils;

import usac.ipc1.Principal;
import usac.ipc1.beans.*;
import usac.ipc1.views.Table;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Date;

public class Utility {

    public void startApp() {

        Principal.contadorClientes = 0;

        int cuentas = 2;
        for (int i = 0; i < 10; i++) {
            Principal.vectorCliente[i] = new Cliente();
            Principal.vectorCliente[i].setNombre("Alejandro_" + i);
            Principal.vectorCliente[i].setApellido("Barrios_" + i);
            Principal.vectorCliente[i].setId(i);
            Principal.vectorCliente[i].setDireccion("Ciudad");
            Principal.vectorCliente[i].setTelefono(12345678);
            if (i % 2 == 0) {
                for (int o = 0; o < cuentas; o++) {
                    Principal.vectorCuenta[Principal.contadorCuenta] = new Cuenta();
                    Principal.vectorCuenta[Principal.contadorCuenta].setCliente(Principal.vectorCliente[i]);
                    Principal.vectorCuenta[Principal.contadorCuenta].setMontoInicial(5000.0);
                    Principal.vectorCuenta[Principal.contadorCuenta].setMontoFinal(5000.0);
                    Principal.vectorCuenta[Principal.contadorCuenta].setNoCuenta(Principal.contadorCuenta);
                    Principal.vectorCuenta[Principal.contadorCuenta].setTipoCuenta(true);
                    Principal.vectorCuenta[Principal.contadorCuenta].setFechaApertura(new Date());
                    Principal.contadorCuenta++;
                }
                cuentas++;
            } else {
                Principal.vectorCuenta[Principal.contadorCuenta] = new Cuenta();
                Principal.vectorCuenta[Principal.contadorCuenta].setCliente(Principal.vectorCliente[i]);
                Principal.vectorCuenta[Principal.contadorCuenta].setMontoInicial(5000.0);
                Principal.vectorCuenta[Principal.contadorCuenta].setMontoFinal(5000.0);
                Principal.vectorCuenta[Principal.contadorCuenta].setNoCuenta(Principal.contadorCuenta);
                Principal.vectorCuenta[Principal.contadorCuenta].setTipoCuenta(true);
                Principal.vectorCuenta[Principal.contadorCuenta].setFechaApertura(new Date());
                Principal.contadorCuenta++;
            }

            Principal.contadorClientes++;
        }

        Principal.contadorEmpleado = 0;

        Principal.contadorAgencia = 0;
        int empleados = 3;
        for (int i = 0; i < 5; i++) {
            Principal.vectorAgencia[i] = new Agencia();
            Principal.vectorAgencia[i].setNombre("Agencia " + (i + 1));
            Principal.vectorAgencia[i].setId(i);
            Principal.vectorAgencia[i].setDireccion("Ciudad");
            Principal.vectorAgencia[i].setTelefono(1234);
            Principal.vectorAgencia[i].setNoCajas(3);
            Principal.vectorAgencia[i].setNoEscritorios(3);
            Principal.vectorAgencia[i].setEfectivo(10000.00);
            Empleado[] empleado = new Empleado[10];
            for (int o = 0; o < empleados; o++) {
                Principal.vectorEmpleado[Principal.contadorEmpleado] = new Empleado();
                Principal.vectorEmpleado[Principal.contadorEmpleado].setId(Principal.contadorEmpleado);
                Principal.vectorEmpleado[Principal.contadorEmpleado].setNombre("Empleado" + Principal.contadorEmpleado);
                Principal.vectorEmpleado[Principal.contadorEmpleado].setApellido("Perez" + Principal.contadorEmpleado);
                Principal.vectorEmpleado[Principal.contadorEmpleado].setTelefono(12345678);
                empleado[o] = Principal.vectorEmpleado[Principal.contadorEmpleado];
                Principal.contadorEmpleado++;
            }
            Principal.vectorAgencia[i].setEmpleados(empleado);
            empleados++;
            Principal.contadorAgencia++;
        }

        Principal.contadorAutobanco = 0;
        for (int i = 0; i < 5; i++) {
            Principal.vectorAutobanco[i] = new Autobanco();
            Principal.vectorAutobanco[i].setNombre("Autobanco " + (i + 1));
            Principal.vectorAutobanco[i].setId(i);
            Principal.vectorAutobanco[i].setDireccion("Ciudad");
            Principal.vectorAutobanco[i].setTelefono(1234);
            Principal.vectorAutobanco[i].setNoCajas(3);
            Principal.vectorAutobanco[i].setNoEscritorios(3);
            Principal.vectorAutobanco[i].setNoCajasAutobanco(3);
            Principal.vectorAutobanco[i].setEfectivo(10000.00);
            Empleado[] empleado = new Empleado[10];
            for (int o = 0; o < 3; o++) {
                Principal.vectorEmpleado[Principal.contadorEmpleado] = new Empleado();
                Principal.vectorEmpleado[Principal.contadorEmpleado].setId(Principal.contadorEmpleado);
                Principal.vectorEmpleado[Principal.contadorEmpleado].setNombre("Empleado" + Principal.contadorEmpleado);
                Principal.vectorEmpleado[Principal.contadorEmpleado].setApellido("Perez" + Principal.contadorEmpleado);
                Principal.vectorEmpleado[Principal.contadorEmpleado].setTelefono(12345678);
                empleado[o] = Principal.vectorEmpleado[Principal.contadorEmpleado];
                Principal.contadorEmpleado++;
            }
            Principal.vectorAutobanco[i].setEmpleados(empleado);
            Principal.contadorAutobanco++;
        }

        Principal.callCenter.setId(0);
        Principal.callCenter.setNombre("Call center");

        Empleado[] empleado = new Empleado[20];
        for (int o = 0; o < 10; o++) {
            Principal.vectorEmpleado[Principal.contadorEmpleado] = new Empleado();
            Principal.vectorEmpleado[Principal.contadorEmpleado].setId(Principal.contadorEmpleado);
            Principal.vectorEmpleado[Principal.contadorEmpleado].setNombre("Empleado" + Principal.contadorEmpleado);
            Principal.vectorEmpleado[Principal.contadorEmpleado].setApellido("Perez" + Principal.contadorEmpleado);
            Principal.vectorEmpleado[Principal.contadorEmpleado].setTelefono(12345678);
            empleado[o] = Principal.vectorEmpleado[Principal.contadorEmpleado];
            Principal.contadorEmpleado++;
        }
        Principal.callCenter.setEmpleados(empleado);


        Principal.contadorCajero = 0;
        for (int i = 0; i < 10; i++) {
            Principal.vectorCajero[i] = new Cajero();
            Principal.vectorCajero[i].setId(i);
            Principal.vectorCajero[i].setUbicacion("Ciudad");
            Principal.vectorCajero[i].setEfectivo(10000.0);
            Principal.vectorCajero[i].setEstado(true);
            Principal.vectorCajero[i].setNoTransaccionesProcesadas(0);
            Principal.contadorCajero++;
        }

        Principal.contadorTransaccion = 0;
        Principal.contadorTarjeta = 0;
        Principal.contadorPrestamo = 0;


    }

    public static JButton lateralButtonProperties(JButton getButton, String title, JFrame frame) {

        getButton.setText(title);
        getButton.setMaximumSize(new Dimension(frame.getWidth(), 35));
        getButton.setPreferredSize(new Dimension(JPanel.WIDTH, 35));
        getButton.setBorder(null);
        getButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        getButton.setHorizontalAlignment(SwingConstants.CENTER);
        getButton.setForeground(Color.decode("#FFFFFF"));
        getButton.setOpaque(true);
        getButton.setBackground(Color.decode("#0D4D4D"));
        getButton.setFocusPainted(false);

        return getButton;

    }

    public static JComboBox buildCombo(JComboBox combo, int selection) {

        switch (selection) {
            case 1:

                for (int i = 0; i < Principal.vectorAgencia.length; i++) {
                    if (Principal.vectorAgencia[i] != null)
                        combo.addItem(Principal.vectorAgencia[i].getNombre());
                }

                break;

            case 2:

                for (int i = 0; i < Principal.vectorAutobanco.length; i++) {
                    if (Principal.vectorAutobanco[i] != null)
                        combo.addItem(Principal.vectorAutobanco[i].getNombre());
                }

                break;

            case 3:

                for (int i = 0; i < Principal.vectorCajero.length; i++) {
                    if (Principal.vectorCajero[i] != null && Principal.vectorCajero[i].isEstado())
                        combo.addItem(Principal.vectorCajero[i].getId());
                }

                break;

            case 4:
                combo.addItem(Principal.callCenter.getNombre());
                break;
        }

        return combo;

    }

    public static void searchAndSetLocation(String name, int selection, Transaccion transaction) {
        Object object;
        switch (selection) {
            case 1:
                for (int i = 0; i < Principal.vectorAgencia.length; i++) {
                    if (Principal.vectorAgencia[i] != null) {
                        if (Principal.vectorAgencia[i].getNombre().equals(name)) {
                            object = Principal.vectorAgencia[i];
                            transaction.setAgencia((Agencia) object);
                        }

                    }
                }
                break;

            case 2:
                for (int i = 0; i < Principal.vectorAutobanco.length; i++) {
                    if (Principal.vectorAutobanco[i] != null) {
                        if (Principal.vectorAutobanco[i].getNombre().equals(name)) {
                            object = Principal.vectorAutobanco[i];
                            transaction.setAutobanco((Autobanco) object);
                        }

                    }
                }
                break;

            case 3:
                for (int i = 0; i < Principal.vectorCajero.length; i++) {
                    if (Principal.vectorCajero[i] != null) {
                        if (Principal.vectorCajero[i].getId() == Integer.parseInt(name)) {
                            object = Principal.vectorCajero[i];
                            transaction.setCajero((Cajero) object);
                        }

                    }
                }
                break;

            case 4:
                transaction.setCallCenter(Principal.callCenter);
                break;
        }

    }

    public static void search(String busqueda, Table.TipoTabla tipoTabla, JFrame frame) {

        boolean founded = false;

        if (!busqueda.equals("")) {

            JDialog dialog = new JDialog();
            dialog.setBounds(new Rectangle(500, 190));
            dialog.setLocationRelativeTo(null);
            dialog.setTitle("Asignar Empleado");
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            JPanel contenedor = new JPanel();
            contenedor.setBackground(Color.WHITE);
            contenedor.setBorder(new EmptyBorder(20, 30, 20, 30));
            BoxLayout boxLayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
            contenedor.setLayout(boxLayout);

            switch (tipoTabla) {
                case AUTOBANCO:
                    for (int i = 0; i < Principal.vectorAutobanco.length; i++) {
                        if (Principal.vectorAutobanco[i] != null) {
                            if (Principal.vectorAutobanco[i].getNombre().equals(busqueda) ||
                                    Integer.toString(Principal.vectorAutobanco[i].getId()).equals(busqueda)) {
                                contenedor.add(new JLabel("Nombre: " + Principal.vectorAutobanco[i].getNombre()));
                                contenedor.add(new JLabel("Direccion: " + Principal.vectorAutobanco[i].getDireccion()));
                                contenedor.add(new JLabel("Telefono: " + Principal.vectorAutobanco[i].getTelefono()));
                                contenedor.add(new JLabel("Efectivo: " + Principal.vectorAutobanco[i].getEfectivo()));
                                contenedor.add(new JLabel("No. Cajas: " + Principal.vectorAutobanco[i].getNoCajas()));
                                contenedor.add(new JLabel("Escritorios: " + Principal.vectorAutobanco[i].getNoEscritorios()));
                                contenedor.add(new JLabel("No. Auto cajas: " + Principal.vectorAutobanco[i].getNoCajasAutobanco()));
                                founded = true;
                            }
                        }
                    }
                    break;

                case AGENCIA:
                    for (int i = 0; i < Principal.vectorAgencia.length; i++) {
                        if (Principal.vectorAgencia[i] != null) {
                            if (Principal.vectorAgencia[i].getNombre().equals(busqueda) ||
                                    Integer.toString(Principal.vectorAgencia[i].getId()).equals(busqueda)) {
                                contenedor.add(new JLabel("Nombre: " + Principal.vectorAgencia[i].getNombre()));
                                contenedor.add(new JLabel("Direccion: " + Principal.vectorAgencia[i].getDireccion()));
                                contenedor.add(new JLabel("Telefono: " + Principal.vectorAgencia[i].getTelefono()));
                                contenedor.add(new JLabel("Efectivo: " + Principal.vectorAgencia[i].getEfectivo()));
                                contenedor.add(new JLabel("No. Cajas: " + Principal.vectorAgencia[i].getNoCajas()));
                                contenedor.add(new JLabel("Escritorios: " + Principal.vectorAgencia[i].getNoEscritorios()));
                                founded = true;
                            }
                        }
                    }
                    break;

                case CLIENTE:
                    for (int i = 0; i < Principal.vectorCliente.length; i++) {
                        if (Principal.vectorCliente[i] != null) {
                            if (Principal.vectorCliente[i].getNombre().equals(busqueda) ||
                                    Integer.toString(Principal.vectorCliente[i].getId()).equals(busqueda)) {
                                contenedor.add(new JLabel("Nombre: " + Principal.vectorCliente[i].getNombre()));
                                contenedor.add(new JLabel("Apellido: " + Principal.vectorCliente[i].getApellido()));
                                contenedor.add(new JLabel("Telefono: " + Principal.vectorCliente[i].getTelefono()));
                                contenedor.add(new JLabel("Direccion: " + Principal.vectorCliente[i].getDireccion()));
                                founded = true;
                            }
                        }
                    }
                    break;

                case CAJERO:
                    for (int i = 0; i < Principal.vectorCajero.length; i++) {
                        if (Principal.vectorCajero[i] != null) {
                            if (Integer.toString(Principal.vectorCajero[i].getId()).equals(busqueda)) {
                                contenedor.add(new JLabel("Id: " + Principal.vectorCajero[i].getId()));
                                contenedor.add(new JLabel("Ubicacion: " + Principal.vectorCajero[i].getUbicacion()));
                                contenedor.add(new JLabel("Efectivo: " + Principal.vectorCajero[i].getEfectivo()));
                                contenedor.add(new JLabel("Transacciones: " + Principal.vectorCajero[i].getNoTransaccionesProcesadas()));
                                founded = true;
                            }
                        }
                    }
                    break;

                case EMPLEADO:
                    for (int i = 0; i < Principal.vectorEmpleado.length; i++) {
                        if (Principal.vectorEmpleado[i] != null) {
                            if (Principal.vectorEmpleado[i].getNombre().equals(busqueda) ||
                                    Integer.toString(Principal.vectorEmpleado[i].getId()).equals(busqueda)) {
                                contenedor.add(new JLabel("Nombre: " + Principal.vectorEmpleado[i].getNombre()));
                                contenedor.add(new JLabel("Apellido: " + Principal.vectorEmpleado[i].getApellido()));
                                contenedor.add(new JLabel("Telefono: " + Principal.vectorEmpleado[i].getTelefono()));
                                founded = true;
                            }
                        }
                    }
                    break;
            }

            dialog.add(contenedor);

            if (founded)
                dialog.setVisible(true);
            else
                JOptionPane.showMessageDialog(frame,
                        "No se encontraron resultados",
                        "Busqueda",
                        JOptionPane.WARNING_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(frame,
                    "Campo vacio",
                    "Busqueda",
                    JOptionPane.WARNING_MESSAGE);
        }

    }

    public static void asignar(int selected, JFrame frame) {
        JDialog dialog = new JDialog();
        dialog.setBounds(new Rectangle(500, 190));
        dialog.setLocationRelativeTo(null);
        dialog.setTitle("Busqueda");
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        JPanel contenedor = new JPanel();
        contenedor.setBackground(Color.WHITE);
        contenedor.setBorder(new EmptyBorder(20, 30, 20, 30));
        BoxLayout boxLayout = new BoxLayout(contenedor, BoxLayout.Y_AXIS);
        contenedor.setLayout(boxLayout);

        JLabel lblAgencias = new JLabel("Asignar en:");
        JComboBox cbAgencias = new JComboBox();
        cbAgencias.setPreferredSize(new Dimension(dialog.getWidth(), 40));
        for (int i = 0; i < Principal.vectorAgencia.length; i++) {
            if (Principal.vectorAgencia[i] != null) {
                cbAgencias.addItem(Principal.vectorAgencia[i].getNombre());
            }
        }
        for (int i = 0; i < Principal.vectorAutobanco.length; i++) {
            if (Principal.vectorAutobanco[i] != null) {
                cbAgencias.addItem(Principal.vectorAutobanco[i].getNombre());
            }
        }
        cbAgencias.addItem(Principal.callCenter.getNombre());

        JButton btnAsignar = new JButton();
        btnAsignar = lateralButtonProperties(btnAsignar, "Asignar", frame);
        btnAsignar.setBackground(Color.decode("#3366cc"));
        btnAsignar.addActionListener(e -> {
            boolean asigned = false;
            for (int i = 0; i < Principal.vectorAgencia.length; i++) {
                if (Principal.vectorAgencia[i] != null) {
                    if (cbAgencias.getSelectedItem().equals(Principal.vectorAgencia[i].getNombre())) {
                        int contadorEmpleado = 0;
                        for (int o = 0; o < Principal.vectorAgencia[i].getEmpleado().length; o++) {
                            if (Principal.vectorAgencia[i].getEmpleado()[o] != null)
                                contadorEmpleado++;
                        }
                        Empleado[] empleado;
                        empleado = Principal.vectorAgencia[i].getEmpleado();
                        empleado[contadorEmpleado + 1] = Principal.vectorEmpleado[selected];
                        Principal.vectorAgencia[i].setEmpleados(empleado);
                        asigned = true;
                    }
                }
            }
            if (!asigned) {
                for (int i = 0; i < Principal.vectorAutobanco.length; i++) {
                    if (Principal.vectorAutobanco[i] != null) {
                        if (cbAgencias.getSelectedItem().equals(Principal.vectorAutobanco[i].getNombre())) {
                            int contadorEmpleado = 0;
                            for (int o = 0; o < Principal.vectorAutobanco[i].getEmpleado().length; o++) {
                                if (Principal.vectorAutobanco[i].getEmpleado()[o] != null)
                                    contadorEmpleado++;
                            }
                            Empleado[] empleado;
                            empleado = Principal.vectorAutobanco[i].getEmpleado();
                            empleado[contadorEmpleado + 1] = Principal.vectorEmpleado[selected];
                            Principal.vectorAutobanco[i].setEmpleados(empleado);
                            asigned = true;
                        }
                    }
                }
            }
            if (!asigned) {
                if (cbAgencias.getSelectedItem().equals(Principal.callCenter.getNombre())) {
                    int contadorEmpleado = 0;
                    for (int i = 0; i < Principal.callCenter.getEmpleado().length; i++) {
                        if (Principal.callCenter.getEmpleado()[i] != null)
                            contadorEmpleado++;
                    }
                    Empleado[] empleado;
                    empleado = Principal.callCenter.getEmpleado();
                    empleado[contadorEmpleado + 1] = Principal.vectorEmpleado[selected];
                    Principal.callCenter.setEmpleados(empleado);
                    asigned = true;
                }
            }

            if (asigned){
                JOptionPane.showMessageDialog(frame,
                        "Asignado exitosamente",
                        "Asignar",
                        JOptionPane.WARNING_MESSAGE);
                dialog.dispose();
            }
        });

        contenedor.add(lblAgencias);
        contenedor.add(cbAgencias);
        contenedor.add(Box.createRigidArea(new Dimension(0, 10)));
        contenedor.add(btnAsignar);

        dialog.add(contenedor);
        dialog.setVisible(true);
    }
}
